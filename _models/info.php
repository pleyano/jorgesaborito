<?php 
defined("INDEX") or die();

function loadTopMenuInfo($user, &$params) {
	
	require_once("includes/db.php");
	require_once("includes/user.php");
	
	$current_user = new User($user);
	$user_id = $current_user->getInfo("id_user");
	
	$info = new Connection();
	$messages = $info->query("select count(*) from js_messages where id_recipent = :1 and recipent_read = 0", array($user_id));
	$params['unread_messages'] = $messages[0][0];
	$params['user_id'] = $user_id;

}

function loadProfileSummaryInfo($user, &$params) {
	
	require_once("includes/db.php");
	require_once("includes/user.php");
	
	$current_user = new User($user);
	$user_id = $current_user->getInfo("id_user");
	
	$info = new Connection();
	$data = $info->query("select * from js_users natural join js_history where id_user = :1 order by date desc limit 1", array($user_id));
	
	$params['profile_name'] = $data[0]['name'];
	$params['profile_weight'] = $data[0]['weight'];
	$params['profile_height'] = $data[0]['height'];
	$params['profile_evaluation'] = (empty($data[0]['evaluation'])) ? "No hay información" : $data[0]['evaluation'];

	
	loadMessages($_SESSION['user'], $params, true, 1, 1);
	
}

function loadAdminProfileSummaryInfo($user, &$params) {
	
	require_once("includes/db.php");
	require_once("includes/user.php");
	
	$info = new Connection();
	$data = $info->query("select username, email, active, regist_date, last_connection, name, birthday, gender, illnesses, allergies, other, evaluation from js_users where id_user = :1", array($user));

	foreach ($data[0] as $key=>$val) {
		$params[$key] = trim($val);
	}
	
}
function loadAdminProfileHistoryInfo($user, &$params) {
	
	require_once("includes/db.php");
	require_once("includes/user.php");
	
	$info = new Connection();
	$history = $info->query("select date_format(date, '%d/%m/%Y') as date, weight, height, round(imc, 2) as imc, round(ptg, 2) as ptg, leg, arm, waist, hips, hunger, activity from js_history where id_user = :1 order by date desc", array($user));
	
	$params['history'] = $history;
	
	$username = $info->query("select username from js_users where id_user = :1", array($user));
	$params['username'] = $username[0]['username'];
	
}

function loadMessages($user, &$params, $inbox, $first, $last) {

	require_once("includes/db.php");
	require_once("includes/user.php");
	
	if ($inbox === true) {
		$column = "id_recipent";
	} else {
		$column = "id_author";
	}
		
	
	$current_user = new User($user);
	$user_id = $current_user->getInfo("id_user");
	
	$info = new Connection();
	$messages = $info->query("select * from js_messages join js_users on js_users.id_user = id_author where deleted <> 1 and " . $column . " = :1 order by date desc", array($user_id));
	
	foreach($messages as $message) {

		$params['messages'][] = array("id_message" => $message['id_message'], "subject" => $message['subject'], "message" => $message['content'], "date" => $message['date'], "id_author" => $message['id_author'], "author" => $message['username'], "read" => $message['recipent_read']);		
		
	}
	
}

function loadProfileProgressInfo($user, &$params) {
	
	require_once("includes/db.php");
	require_once("includes/user.php");
	
	$current_user = new User($user);
	$user_id = $current_user->getInfo("id_user");
	
	$info = new Connection();
	$history = $info->query("select round(weight,2) as weight, round(ptg,2) as ptg, round(imc, 2) as imc, date_format(date,'%m/%d/%Y') as date from js_history where id_user = :1", array($user_id));
	
	foreach ($history as $hist_row) {
		$params['chart_imc_imcs'][] = $hist_row['imc'];
		$params['chart_ptg_ptgs'][] = $hist_row['ptg'];
		$params['chart_weight_weights'][] = $hist_row['weight'];
		$params['chart_imc_dates'][] = $params['chart_ptg_dates'][] = $params['chart_weight_dates'][] = $hist_row['date'];
	}
	
	$params['chart_imc_miny'] = min($params['chart_imc_imcs']) * 0.8;
	$params['chart_imc_maxy'] = max($params['chart_imc_imcs']) * 1.2;
	
	$params['chart_ptg_miny'] = min($params['chart_ptg_ptgs']) * 0.8;
	$params['chart_ptg_maxy'] = max($params['chart_ptg_ptgs']) * 1.2;
	
	$params['chart_weight_miny'] = min($params['chart_weight_weights']) * 0.8;
	$params['chart_weight_maxy'] = max($params['chart_weight_weights']) * 1.2;

}

function loadPreferencesInfo($user, &$params) {

	require_once("includes/db.php");
	require_once("includes/user.php");
	
	$current_user = new User($user);
	$user_id = $current_user->getInfo("id_user");
	
	$info = new Connection();
	$preferences = $info->query("select email, avatar from js_users where id_user = :1", array($user_id));
	
	$params['user_preferences']['username'] = $user;
	$params['user_preferences']['email'] = $preferences[0]["email"];
	$params['user_preferences']['avatar'] = $preferences[0]["avatar"];
	$params['user_preferences']['avatar_path'] = Base::config("SITE", "upload_path") . "/" . $preferences[0]["avatar"];
	
}

function checkPassword($user, $password) {
	require_once("includes/user.php");
	$current_user = new User($user, $password);
	return $current_user->authenticate();
}

function loadData($user, &$params) {
	require_once("includes/db.php");
	require_once("includes/user.php");
	
	$current_user = new User($user);
	$user_id = $current_user->getInfo("id_user");
	
	$info = new Connection();
	$data = $info->query("select hunger, activity, leg, arm, waist, hips, height, weight from js_history where id_user = :1 order by date desc limit 1", array($user_id));
	
	$params['data']['hunger'] = $data[0]['hunger'];
	$params['data']['activity'] = $data[0]['activity'];
	$params['data']['leg'] = $data[0]['leg'];
	$params['data']['arm'] = $data[0]['arm'];
	$params['data']['waist'] = $data[0]['waist'];
	$params['data']['hips'] = $data[0]['hips'];
	$params['data']['height'] = $data[0]['height'];
	$params['data']['weight'] = $data[0]['weight'];

	$personal_data = $info->query("select name, birthday, gender, illnesses, allergies, other from js_users where id_user = :1", array($user_id));
	
	$params['data']['name'] = $personal_data[0]['name'];
	$params['data']['birthday'] = date("d/m/Y", strtotime($personal_data[0]['birthday']));
	$params['data']['gender'] = $personal_data[0]['gender'];
	$params['data']['illnesses'] = $personal_data[0]['illnesses'];
	$params['data']['allergies'] = $personal_data[0]['allergies'];
	$params['data']['other'] = $personal_data[0]['other'];
	
}

function loadUsers($filter = "", &$params) {
	require_once("includes/db.php");
	require_once("includes/user.php");
	
	$info = new Connection();
	$data = $info->query("select * from js_users where username like '%" . $filter . "%'", array());
	
	$params['users'] = $data;
	
}

function getUserName($id_user) {
	require_once("includes/db.php");
	require_once("includes/user.php");
	
	$info = new Connection();
	$data = $info->query("select username from js_users where id_user = :1", array($id_user));
	
	return $data[0]['username'];
}

function daysSinceLastUpdate($user) {
	require_once("includes/db.php");
	require_once("includes/user.php");
	
	$info = new Connection();
	$data = $info->query("select datediff(now(), (select date from js_history where id_user = :1 order by date desc limit 1)) as diff", array($user));
	
	if (sizeof($data) > 0) {
		return $data[0]['diff'];
	} else {
		return -1;
	}

}

function loadAdminProgramsInfo(&$params) {

	require_once("includes/db.php");
	
	$info = new Connection();
	$programs = $info->query("select * from js_programs join js_users on js_programs.id_user = js_users.id_user", array());
	
	foreach ($programs as $program) {
		$params['programs'][] = array("id_program" => $program['id_program'], "date" => $program['date'], "username" => $program['username'], "status" => $program['payment_status'], "pdf" => (empty($program['asociated_pdf']) ? "0" : $program['asociated_pdf']), "diet" => (empty($program['diet']) ? "0" : "1"), "activity" => (empty($program['activity']) ? "0" : "1"));
	}
}

function loadProgramsInfo($user, &$params) {

	require_once("includes/db.php");
	
	$info = new Connection();
	
	if (!empty($user)) {
		require_once("includes/user.php");
		
		$current_user = new User($user);
		$user_id = $current_user->getInfo("id_user");

		$programs = $info->query("select * from js_programs where id_user = :1", array($user_id));
	} else {
		$programs = $info->query("select * from js_programs", array());
	}
	
	foreach ($programs as $program) {
		$params['programs'][] = array("id_program" => $program['id_program'], "date" => $program['date'], "username" => getUserName($program['id_user']), "status" => $program['payment_status'], "pdf" => (empty($program['asociated_pdf']) ? "0" : $program['asociated_pdf']), "diet" => (empty($program['diet']) ? "0" : "1"), "activity" => (empty($program['activity']) ? "0" : "1"));
	}
}

function loadProgramSummary($user, $id_program, &$params) {

	require_once("includes/db.php");
	
	$info = new Connection();
	if (!empty($user)) {
		require_once("includes/user.php");
		
		$current_user = new User($user);
		$user_id = $current_user->getInfo("id_user");
		
		$program = $info->query("select * from js_programs where id_user = :1 and id_program = :2", array($user_id, $id_program));
	} else {
		$program = $info->query("select * from js_programs where id_program = :1", array($id_program));
	}
	$params['program_summary'] = $program[0];
	
}


function loadProgramDietInfo($user, $id_program, &$params) {

	require_once("includes/db.php");
	$info = new Connection();
	if (!empty($user)) {
		require_once("includes/user.php");
		
		$current_user = new User($user);
		$user_id = $current_user->getInfo("id_user");
		
		$diet = $info->query("select * from js_programs join js_diets on diet = id_diet where id_user = :1 and id_program = :2", array($user_id, $id_program));
	} else {
		$diet = $info->query("select * from js_programs join js_diets on diet = id_diet where id_program = :1", array($id_program));
	}
	$params['program_diet'] = $diet[0];
	
	loadProgramSummary($user, $id_program, &$params);
	
}


function loadProgramActivityInfo($user, $id_program, &$params) {

	require_once("includes/db.php");
	require_once("includes/user.php");
	
	$current_user = new User($user);
	$user_id = $current_user->getInfo("id_user");
	
	$info = new Connection();
	$diet = $info->query("select * from js_programs join js_diets on diet = id_diet where id_user = :1 and id_program = :2", array($user_id, $id_program));
	$params['program_diet'] = $diet[0];
	
	loadProgramSummary($user, $id_program, &$params);
	
}

?>