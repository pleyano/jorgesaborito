<?php 
defined("INDEX") or die();

function updatePreferences($user, $preferences) {
	$fields = array_keys($preferences);
	$string = "update js_users set ";
	for ($i = 0; $i < sizeof($preferences) - 1; $i++) {
		$string .= $fields[$i] . " = :" . ($i+1) . ", ";
	}
	$string .= $fields[$i] . " = :" . ($i+1);
	
	$string .= " where id_user = :" . ($i+2);

	require_once("includes/user.php");
	
	$current_user = new User($user);
	$user_id = $current_user->getInfo("id_user");
	
	$preferences[] = $user_id;
	
	$update = new Connection();
	$update->nonQuery($string, $preferences);
	
}

function sendMessage($subject, $content, $author, $recipent) {
	
	require_once("includes/user.php");
	require_once("includes/db.php");
	
	$current_user = new User($author);
	$user_id = $current_user->getInfo("id_user");
	
	$recipent_user = new User($recipent);
	$recipent_id = $current_user->getInfo("id_user");
	
	$insertion = new Connection();
	$insertion->nonQuery("insert into js_messages set subject = :1, content = :2, id_author = :3, id_recipent = :4, date = now()", array($subject, $content, $user_id, $recipent_id));
	
}

function updateHistory($user, $data) {

	$fields = array_keys($data);
	
	require_once("includes/user.php");
	
	$update = new Connection();
	
	$current_user = new User($user);
	$user_id = $current_user->getInfo("id_user");
	
	$existing_row = $update->query("select 1 from js_history where id_user = :1 and date(date) = date(now())", array($user_id));
	
	if (empty($existing_row)) {
		
		$string = "insert into js_history set ";
		for ($i = 0; $i < sizeof($data) - 1; $i++) {
			$string .= $fields[$i] . " = :" . ($i+1) . ", ";
		}
		
		$string .= $fields[$i] . " = :" . ($i+1);
		$string .= ", id_user = :" . ($i+2);
		$string .= ", imc = (weight)/(height*height/10000), ptg = leg + arm + hips + waist";
		
	} else {
		
		$string = "update js_history set ";
		for ($i = 0; $i < sizeof($data) - 1; $i++) {
			$string .= $fields[$i] . " = :" . ($i+1) . ", ";
		}
		
		$string .= $fields[$i] . " = :" . ($i+1);
		$string .= ", imc = (weight)/(height*height/10000), ptg = leg + arm + hips + waist";
		$string .= " where id_user = :" . ($i+2);
		$string .= " and date(date) = date(now())";
		
	}
	
	$data[] = $user_id;
	
	$update->nonQuery($string, $data);

}

function registerUser($username, $password, $email, $birthday_day, $birthday_month, $birthday_year) {
	
	require_once("includes/user.php");
	
	$insertion = new Connection();
	$existing_user = $insertion->query("select 1 from js_users where username = :1", array($username));
	
	if (empty($existing_user)) {
	
		$insertion->nonQuery("insert into js_users set username = :1, password = :2, email = :3, active = :4, birthday = :5", array($username, md5($password), $email, md5(rand()), $birthday_year . "-" . $birthday_month . "-" . $birthday_day));
	
	}
	
}

function actDeact($user) {
	
	require_once("includes/user.php");
	
	$connection = new Connection();
	$blocked_user = $connection->query("select 1 from js_users where id_user = :1 and active = '0'", array($user));
	
	if (!empty($blocked_user)) {
	
		$connection->nonQuery("update js_users set active = md5(now()) where id_user = :1", array($user));
	
	} else {
	
		$connection->nonQuery("update js_users set active = 0 where id_user = :1", array($user));
	
	}
	
}

function deleteUser($user) {
	
	require_once("includes/user.php");
	
	$connection = new Connection();
	$connection->nonQuery("delete from js_users where id_user = :1", array($user));

	
}

function insertDiet($diet_form) {
	
	require_once("includes/db.php");
	
	$days = array();
	$days[0] = $diet_form['day0time0'] . "|" . $diet_form['day0time1'] . "|" . $diet_form['day0time2'] . "|" . $diet_form['day0time3'] . "|" . $diet_form['day0time4'] . "|" . $diet_form['day0time5'];
	$days[1] = $diet_form['day1time0'] . "|" . $diet_form['day1time1'] . "|" . $diet_form['day1time2'] . "|" . $diet_form['day1time3'] . "|" . $diet_form['day1time4'] . "|" . $diet_form['day1time5'];
	$days[2] = $diet_form['day2time0'] . "|" . $diet_form['day2time1'] . "|" . $diet_form['day2time2'] . "|" . $diet_form['day2time3'] . "|" . $diet_form['day2time4'] . "|" . $diet_form['day2time5'];
	$days[3] = $diet_form['day3time0'] . "|" . $diet_form['day3time1'] . "|" . $diet_form['day3time2'] . "|" . $diet_form['day3time3'] . "|" . $diet_form['day3time4'] . "|" . $diet_form['day3time5'];
	$days[4] = $diet_form['day4time0'] . "|" . $diet_form['day4time1'] . "|" . $diet_form['day4time2'] . "|" . $diet_form['day4time3'] . "|" . $diet_form['day4time4'] . "|" . $diet_form['day4time5'];
	$days[5] = $diet_form['day5time0'] . "|" . $diet_form['day5time1'] . "|" . $diet_form['day5time2'] . "|" . $diet_form['day5time3'] . "|" . $diet_form['day5time4'] . "|" . $diet_form['day5time5'];
	$days[6] = $diet_form['day6time0'] . "|" . $diet_form['day6time1'] . "|" . $diet_form['day6time2'] . "|" . $diet_form['day6time3'] . "|" . $diet_form['day6time4'] . "|" . $diet_form['day6time5'];
	
	$connection = new Connection();
	
	$last_diet = $connection->query("select max(id_diet) + 1 from js_diets", array());
	
	$connection->nonQuery("insert into js_diets set day1 = :1, day2 = :2, day3 = :3, day4 = :4, day5 = :5, day6 = :6, day7 = :7, 24hours1 = :8, 24hours2 = :9, 24hours3 = :10, 24hours4 = :11, 24hours5 = :12, 24hours6 = :13, 24hours7 = :14, observations = :15", array_merge($days, array($diet_form['24hours_day1'], $diet_form['24hours_day2'], $diet_form['24hours_day3'], $diet_form['24hours_day4'], $diet_form['24hours_day5'], $diet_form['24hours_day6'], $diet_form['24hours_day7'], $diet_form['observations'])));
	
	return $last_diet[0][0];
	
}

function insertActivity($activity_form) {
	
	require_once("includes/db.php");
	$activity_names = "";
	
	foreach ($activity_form['activity_name'] as $activity) {
		$activity_names .= $activity . "|";
	}
	
	if (strlen($activity_names) > 0) {
		$activity_names = substr($activity_names, 0, strlen($activity_names) - 1);
	}
	
	$activity_durations = "";
	
	foreach ($activity_form['activity_duration'] as $activity) {
		$activity_durations .= $activity . "|";
	}
	
	if (strlen($activity_durations) > 0) {
		$activity_durations = substr($activity_durations, 0, strlen($activity_durations) - 1);
	}
	
	$activity_frequencies = "";
	
	foreach ($activity_form['activity_frequency'] as $activity) {
		$activity_frequencies .= $activity . "|";
	}
	
	if (strlen($activity_frequencies) > 0) {
		$activity_frequencies = substr($activity_frequencies, 0, strlen($activity_frequencies) - 1);
	}
	
	$connection = new Connection();
	
	$last_activity = $connection->query("select max(id_activity) + 1 from js_activities", array());
	
	$connection->nonQuery("insert into js_activities set activity = :1, duration = :2, frequency = :3, observations = :4", array($activity_names, $activity_durations, $activity_frequencies, $activity_form['observations']));
	
	return $last_activity[0][0];
	
}

function insertProgram($last_diet, $last_activity, $user) {
	
	require_once("includes/user.php");
	require_once("includes/db.php");
	
	$current_user = new User($user);
	$user_id = $current_user->getInfo("id_user");
	
	$connection = new Connection();
	$connection->nonQuery("insert into js_programs set diet = :1, activity = :2, id_user = :3, date = now(), payment_status = 0, asociated_pdf = '0'", array($last_diet, $last_activity, $user_id));
	
}

function confirmPayment($status, $program_id) {
	
	require_once("includes/db.php");
	
	$connection = new Connection();
	$connection->nonQuery("update js_programs set payment_status = :1 where id_program = :2", array($status, $program_id));
	
}

function updateUploadedPDF($newname, $id_program) {

	require_once("includes/db.php");
	
	$connection = new Connection();
	$connection->nonQuery("update js_programs set asociated_pdf = :1 where id_program = :2", array($newname, $id_program));
	
}

?>