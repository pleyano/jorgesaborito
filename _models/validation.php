<?php
defined("INDEX") or die();

function validate_registration(&$array_fields) {

	foreach ($array_fields as $key=>$value) {
		if(empty($value)) { 
			 unset($array_fields[$key]);
		}
	}
	require_once("includes/gump_extended.php");
	
	$rules = array(
		"username"=>"required|min_len,6|max_len,30|username",
		"password"=>"required|min_len,6|max_len,30|alpha_numeric",
		"email"=>"required|valid_email",
		"birthday_year"=>"required|numeric|min_val,1900|max_val," . date('Y'),
		"birthday_month"=>"required|numeric|min_val,1|max_val,12",
		"birthday_day"=>"required|numeric|min_val,1|max_val,31"
	);
	
	$validator = new GUMP_extended();
	$status = $validator->validate($array_fields, $rules);
	
	return $status;
	
}

function validate_message(&$array_fields) {

	foreach ($array_fields as $key=>$value) {
		if(empty($value)) { 
			 unset($array_fields[$key]);
		}
	}
	require_once("includes/gump_extended.php");
	
	$rules = array(
		"subject" => "required|min_len,1|max_len,255",
		"content" => "required|min_len,1|max_len,8000"
	);
	
	$validator = new GUMP_extended();
	$status = $validator->validate($array_fields, $rules);
	
	return $status;
	
}

function validate_preferences(&$array_fields) {

	foreach ($array_fields as $key=>$value) {
		if(empty($value)) { 
			 unset($array_fields[$key]);
		}
	}
	require_once("includes/gump_extended.php");
	
	
	$rules = array(
		"password"=>"min_len,6|max_len,30|alpha_numeric",
		"email"=>"valid_email",
		"avatar"=>"min_len,4|max_len,255|image_ext"
	);
	
	$validator = new GUMP_extended();
	$status = $validator->validate($array_fields, $rules);
	
	return $status;
	
}

function validate_data(&$array_fields) {

	foreach ($array_fields as $key=>$value) {
		if(empty($value)) { 
			 unset($array_fields[$key]);
		}
	}
	require_once("includes/gump_extended.php");
	
	$rules = array(
		"birthday"=>"exact_len,10",
		"gender"=>"exact_len,1|gender",
		"activity"=>"required|numeric|min_val,1|max_val,4",
		"hunger"=>"required|numeric|min_val,1|max_val,10",
		"weight"=>"required|numeric|min_val,1|max_val,300",
		"height"=>"required|numeric|min_val,1|max_val,250",
		"name"=>"max_len,255",
		"leg"=>"required|numeric|min_val,10|max_val,100",
		"arm"=>"required|numeric|min_val,10|max_val,100",
		"waist"=>"required|numeric|min_val,15|max_val,200",
		"hips"=>"required|numeric|min_val,15|max_val,200",
		"illnesses"=>"max_len,1024",
		"allergies"=>"max_len,1024",
		"other"=>"max_len,1024"
	);
	
	$validator = new GUMP_extended();
	$status = $validator->validate($array_fields, $rules);

	return $status;
	
}

function validate_diet(&$array_fields) {
	$n = 0;
	foreach ($array_fields as $key=>$value) {
		$n++;
		if ($n > 42) break; 
		if(empty($value)) { 
			 $array_fields[$key] = "X";
		}
	}
	
	return true; // NO VALIDATION HERE
	
}

function valid_fields($fields, $valid_names) {
	foreach ($fields as $name) {
		if (!in_array($name, $valid_names)) {
			return false;
		}
	}
	return true;
}
?>