<?php 
defined("INDEX") or die();

function getWordpressPosts($server, $db, $user, $password, &$params) {

	$connection;

	try {

		$connection = new PDO('mysql:host=' . $server . ';dbname=' . $db, $user, $password);

	} catch (PDOException $e) {
		
		$connection = false;
		
	}
	
	$query = $connection->prepare("select post_title, post_content, guid from wp_posts where post_status = 'publish' order by post_date desc");
	
	$query->execute();
	
	$params['posts'] = $query->fetchAll();
	
	
}

?>