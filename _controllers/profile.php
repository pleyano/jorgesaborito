<?php

class Profile {
	
	function __construct() {
		if (!isset($_SESSION['user'])) {
			Base::view("index", "Login");
		}	
		Base::addStyle("user.css");
	}
	

	function Index() {
		if (isset($_SESSION['user'])) {
			include_once("_models/info.php");
			loadProfileSummaryInfo($_SESSION['user'], $params);
			include("_views/user/index.php");
		}
	}
	
	function Header() {
		if (isset($_SESSION['user']))
			include("_views/user/header.php");
	}
	
	function Features() {
		if (isset($_SESSION['user'])) 
			include("_views/modules/userFeatures.php");
	}

	function Logout() {
		
		if (isset($_SESSION['user'])) {
			require_once("includes/user.php");
			$current_user = new User($_SESSION['user']);
			$current_user->logout();
			
		}
		Base::view("index", "Index");
		
	}
	
	function Inbox() {
		if (isset($_SESSION['user'])) {
			
			include("_models/info.php");
			
			loadMessages($_SESSION['user'], $params, true, 1, 1);
			
			include("_views/user/inbox.php");
			
			
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			Base::addScript("mailbox.js");
		}
	}
	
	function Outbox() {
		if (isset($_SESSION['user'])) {
		
			include("_models/info.php");
			
			loadMessages($_SESSION['user'], $params, false, 1, 1);
			
			include("_views/user/outbox.php");
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			Base::addScript("mailbox.js");
		}
	}
	
	function Write() {
		if (isset($_SESSION['user'])) {
			if (isset($_POST['send_message'])) {
				unset($_POST['send_message']);
				include_once("_models/validation.php");
				if (valid_fields(array_keys($_POST), array("content", "subject"))) {
					if(validate_message($_POST)) {
						include_once("_models/dbchanges.php");
						sendMessage($_POST['subject'], $_POST['content'], $_SESSION['user'], "pleyano");
					}
				}
			}
			include("_views/user/write.php");
			Base::addStyle("jquery.wysiwyg.css");
			Base::addStyle("form.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-migrate-1.2.1.js");
			Base::addScript("form.js");
			Base::addScript("jquery.wysiwyg.js");
			Base::addScript("jquery.wysiwyg-default.js");
		}
	}
	
	function Progress() {
		if (isset($_SESSION['user'])) {
			Base::addScript("polychart2.standalone.js");
			require("_models/info.php");
			loadProfileProgressInfo($_SESSION['user'], $params);
			include("_views/user/progress.php");
		}
	}
	
	function Newprogram() {
		if (isset($_SESSION['user'])) {
			include("_views/user/newprogram.php");
		}
	}
	
	function Newprogram01() {
		if (isset($_SESSION['user'])) {
			$params['form_action'] = "index.php?controller=profile&amp;action=Newprogram04";
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			Base::addStyle("form.css");
			Base::addStyle("program.css");
			include("_views/user/newprogram_step1.php");
		}
	}
	
	function Newprogram02() {
		if (isset($_SESSION['user'])) {	
			$params['form_action'] = "index.php?controller=profile&amp;action=Newprogram03";
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			Base::addStyle("form.css");
			Base::addStyle("program.css");
			include("_views/user/newprogram_step1.php");
		}
	}
	
	function Newprogram03() {
		if (isset($_SESSION['user'])) {
			if (isset($_POST['form_diet_submit'])) {
				unset($_POST['form_diet_submit']);
				$_SESSION['diet_form'] = $_POST;
			}
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery.dd.min.js");
			Base::addScript("form.js");
			Base::addStyle("dd.css");
			Base::addStyle("form.css");
			Base::addStyle("program.css");
			include("_views/user/newprogram_step2.php");
		}
	}
	
	function Newprogram04() {
		if (isset($_SESSION['user'])) {
			
			$valid = true;
			$last_diet = null;
			$last_activity = null;
		
			if (isset($_POST['form_activity_submit'])) { // Only activity
				unset($_POST['form_activity_submit']);
				$activity_form = $_POST;
				if (isset($_SESSION['diet_form'])) { // Diet + Activity
					$diet_form = $_SESSION['diet_form'];
					unset($_SESSION['diet_form']);
				}
			} elseif (isset($_POST['form_diet_submit'])) { // Only diet
				unset($_POST['form_diet_submit']);
				$diet_form = $_POST;
			} else {
				$valid = false;
			}
			
			if (isset($diet_form) && $valid === true) {
				require_once("_models/validation.php");
				if (valid_fields(array_keys($diet_form), array(
					"day0time0", "day0time1", "day0time2", "day0time3", "day0time4", "day0time5", "day0time6", 
					"day1time0", "day1time1", "day1time2", "day1time3", "day1time4", "day1time5", "day1time6", 
					"day2time0", "day2time1", "day2time2", "day2time3", "day2time4", "day2time5", "day2time6", 
					"day3time0", "day3time1", "day3time2", "day3time3", "day3time4", "day3time5", "day3time6",
					"day4time0", "day4time1", "day4time2", "day4time3", "day4time4", "day4time5", "day4time6", 
					"day5time0", "day5time1", "day5time2", "day5time3", "day5time4", "day5time5", "day5time6",
					"day6time0", "day6time1", "day6time2", "day6time3", "day6time4", "day6time5", "day6time6",
					"24hours_day1", "24hours_day2", "24hours_day3", "24hours_day4", "24hours_day5", "24hours_day6", "24hours_day7", "observations"))) {
					if (validate_diet($diet_form)) {
						require_once("_models/dbchanges.php");
						$last_diet = insertDiet($diet_form);
					}
				} else {
					$valid = false;
				}
			}
			
			if (isset($activity_form) && $valid === true) { // MISSING ACTIVITY DATA VALIDATION
				require_once("_models/validation.php");
				if (valid_fields(array_keys($activity_form), array("activity_duration", "activity_name", "activity_frequency", "observations"))) {
					require_once("_models/dbchanges.php");
					$last_activity = insertActivity($activity_form);
				}
			}
			
			require_once("_models/dbchanges.php");
			
			insertProgram($last_diet, $last_activity, $_SESSION['user']);
			
			include("_views/user/newprogram_confirm.php");
		}
	}
	
	function Data() {
		if (isset($_SESSION['user'])) {
			
			require_once("_models/info.php");
			require_once("_models/validation.php");
			if (isset($_POST['update_data'])) {
				if (valid_fields(array_keys($_POST), array("update_data", "weight", "height", "leg", "arm", "waist", "hips", "hunger", "activity", "birthday", "gender", "illnesses", "allergies", "other", "name"))) {

					unset($_POST['update_data']);
					require_once("_models/info.php");
					if (validate_data($_POST) === true) {
					
						require_once("_models/dbchanges.php");
						
						$array_preferences = array();
						if (isset($_POST['name'])) $array_preferences['name'] = $_POST['name'];
						if (isset($_POST['gender'])) $array_preferences['gender'] = $_POST['gender'];
						if (isset($_POST['birthday'])) $array_preferences['birthday'] = date("Y-m-d", strtotime($_POST['birthday']));
						if (isset($_POST['allergies'])) $array_preferences['allergies'] = $_POST['allergies'];
						if (isset($_POST['illnesses'])) $array_preferences['illnesses'] = $_POST['illnesses'];
						if (isset($_POST['other'])) $array_preferences['other'] = $_POST['other'];
						
						updatePreferences($_SESSION['user'], $array_preferences);
						
						$array_data = array();
						if (isset($_POST['weight'])) $array_data['weight'] = $_POST['weight'];
						if (isset($_POST['height'])) $array_data['height'] = $_POST['height'];
						if (isset($_POST['leg'])) $array_data['leg'] = $_POST['leg'];
						if (isset($_POST['arm'])) $array_data['arm'] = $_POST['arm'];
						if (isset($_POST['waist'])) $array_data['waist'] = $_POST['waist'];
						if (isset($_POST['hips'])) $array_data['hips'] = $_POST['hips'];
						if (isset($_POST['hunger'])) $array_data['hunger'] = $_POST['hunger'];
						if (isset($_POST['activity'])) $array_data['activity'] = $_POST['activity'];
						
						updateHistory($_SESSION['user'], $array_data);
						
					}
				}
			}
			
			loadData($_SESSION['user'], $params);
			
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			Base::addScript("jquery.dd.min.js");
			Base::addScript("form.js");
			Base::addStyle("form.css");
			Base::addStyle("dd.css");
			include("_views/user/data.php");
		}
	}
	
	function Preferences() {
		if (isset($_SESSION['user'])) {
			require_once("_models/info.php");
			require_once("_models/validation.php");
			$params['error'] = "";
			loadPreferencesInfo($_SESSION['user'], $params);
			if (isset($_POST['update_preferences']) && valid_fields(array_keys($_POST), array("update_preferences", "my_password", "password", "my_new_password2", "email", "my_email2", "avatar"))) {
				unset($_POST['update_preferences']);
				unset($_POST['my_email2']);
				unset($_POST['my_new_password2']);
				if (checkPassword($_SESSION['user'], $_POST['my_password'])) {
					unset($_POST['my_password']);
					if (validate_preferences($_POST) === true) {
						require_once("_models/dbchanges.php");
						if (isset($_POST['password'])) $_POST['password'] = md5($_POST['password']);
						updatePreferences($_SESSION['user'], $_POST);
					}
				} else {
					$params['error'] = '<h4 class="login_error">Tu contraseña es incorrecta</h4>';
				}
				$params['user_preferences']['username'] = $_SESSION['user'];
				$params['user_preferences']['avatar_path'] = Base::config("SITE", "upload_path") . "/" . "jorge.jpg";
			}
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			Base::addScript("jquery.dd.min.js");
			Base::addScript("form.js");
			Base::addStyle("form.css");
			Base::addStyle("dd.css");
			include("_views/user/preferences.php");
		}
	}
	
	function Programs() {
		if (isset($_SESSION['user'])) {
			include_once("_models/info.php");
			
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addStyle("form.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			loadProgramsInfo($_SESSION['user'], $params);
			
			include_once("_views/user/programs.php");
		}
	}
	
	function ProgramSummary() {
		if (isset($_SESSION['user'])) {
			include_once("_models/info.php");
			
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addStyle("form.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			loadProgramSummary($_SESSION['user'], $_GET['id_program'], $params);
			
			include_once("_views/user/program.php");
		}
	}
	
	function ProgramDiet() {
		if (isset($_SESSION['user'])) {
			include_once("_models/info.php");
			
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addStyle("form.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			loadProgramDietInfo($_SESSION['user'], $_GET['id_program'], $params);
			
			include_once("_views/user/diet.php");
		}
	}
	
	function ProgramActivity() {
		if (isset($_SESSION['user'])) {
			include_once("_models/info.php");
			
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addStyle("form.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			loadProgramDietInfo($_SESSION['user'], $_GET['id_program'], $params);
			
			include_once("_views/user/activity.php");
		}
	}
	
	function ConfirmPayment() {
		if (isset($_SESSION['user'])) {
			include_once("_models/dbchanges.php");
			
			confirmPayment("1", $_GET['id_program']);
			
			Base::view("profile", "ProgramSummary");
		}
	}
}

?>
