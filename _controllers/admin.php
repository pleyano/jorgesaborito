<?php

class Admin {
	
	function __construct() {
		if (!isset($_SESSION['admin'])) {
			Base::view("index", "Login");
		}	
		Base::addStyle("user.css");
		Base::addStyle("admin.css");
	}
	

	function Index() {
		if (isset($_SESSION['admin'])) {
			include_once("_models/info.php");
			loadProfileSummaryInfo($_SESSION['user'], $params);
			include("_views/admin/index.php");
		}
	}

	function Features() {
		if (isset($_SESSION['admin'])) {
			include_once("_views/modules/adminFeatures.php");
		}
	}

	function Users() {
		if (isset($_SESSION['admin'])) {
			Base::addStyle("form.css");
			include_once("_models/info.php");
			
			if (isset($_POST['filter'])) {
				loadUsers($_POST['filter'], $params);
				$params['filter'] = $_POST['filter'];
			} else {
				loadUsers("", $params);
			}
			
			include_once("_views/admin/users.php");
		}
	}
	
	function Inbox() {
		if (isset($_SESSION['user'])) {
			
			include("_models/info.php");
			
			loadMessages($_SESSION['user'], $params, true, 1, 1);
			
			include("_views/admin/inbox.php");
			
			
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			Base::addScript("mailbox.js");
		}
	}
	

	function Write() {
		if (isset($_SESSION['admin'])) {
		
			include_once("_models/info.php");
			
			if (isset($_POST['send_message'])) {
				unset($_POST['send_message']);
				include_once("_models/validation.php");
			
				if (valid_fields(array_keys($_POST), array("content", "subject", "input_username"))) {
					if(validate_message($_POST)) {
						include_once("_models/dbchanges.php");
						sendMessage($_POST['subject'], $_POST['content'], $_SESSION['user'], $_POST['input_username']);
					}
				}
			}
			
			if (isset($_GET['id_dest'])) {
				$params['dest_username'] = getUserName($_GET['id_dest']);
			}
			
			Base::addStyle("jquery.wysiwyg.css");
			Base::addStyle("form.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-migrate-1.2.1.js");
			Base::addScript("form.js");
			Base::addScript("jquery.wysiwyg.js");
			Base::addScript("jquery.wysiwyg-default.js");
			include_once("_views/admin/write.php");
		}
	}

	function User() {
		if (isset($_SESSION['admin'])) {
			require_once("_models/info.php");
			loadAdminProfileSummaryInfo($_GET['id_user'], $params);
			include_once("_views/admin/user.php");
		}
	}

	function History() {
		if (isset($_SESSION['admin'])) {
			require_once("_models/info.php");
			loadAdminProfileHistoryInfo($_GET['id_user'], $params);
			include_once("_views/admin/history.php");
		}
	}

	function Actions() {
		if (isset($_SESSION['admin'])) {
			require_once("_models/info.php");
			loadAdminProfileSummaryInfo($_GET['id_user'], $params);
			include_once("_views/admin/actions.php");
		}
	}
	
	function ActDeact() {
		if (isset($_SESSION['admin'])) {
			require_once("_models/dbchanges.php");
			actDeact($_GET['id_user']);
			Base::view("admin", "Actions");
		}
	}
	
	function DeleteUser() {
		if (isset($_SESSION['admin'])) {
			require_once("_models/dbchanges.php");
			deleteUser($_GET['id_user']);
			Base::view("admin", "Users");
		}
	}
	
	
	function SelectContact() {
		if (isset($_SESSION['admin'])) {
			
			include_once("_models/info.php");
			
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			Base::addScript("jquery.dd.min.js");
			Base::addScript("form.js");
			Base::addStyle("form.css");
			Base::addStyle("dd.css");
			
			if (isset($_POST['filter'])) {
				loadUsers($_POST['filter'], $params);
				$params['filter'] = $_POST['filter'];
			} else {
				loadUsers("", $params);
			}
			
			include_once("_views/admin/selectContact.php");
		}
	}
	
	function SelectPDF() {
		if (isset($_SESSION['admin'])) {
			
			include_once("_models/dbchanges.php");
			
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			Base::addStyle("form.css");
			print_r($_FILES['file_upload']);
			if (isset($_FILES['file_upload'])) {
				
				$newname = "uploads/" . md5($_FILES["file_upload"]["tmp_name"] . microtime()) . ".pdf";
				move_uploaded_file($_FILES["file_upload"]["tmp_name"], $newname);
				
				updateUploadedPDF($newname, $_GET['id_program']);
				
				echo "<script>window.close()</script>";
			}
			
			include_once("_views/admin/selectPDF.php");
		}
	}
	
	function Programs() {
		if (isset($_SESSION['admin'])) {
			include_once("_models/info.php");
			
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addStyle("form.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			
			loadProgramsInfo(null, $params);
			
			include_once("_views/admin/programs.php");
		}
	}
	
	function ProgramSummary() {
		if (isset($_SESSION['admin'])) {
			include_once("_models/info.php");
			
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			Base::addStyle("form.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			loadProgramSummary(null, $_GET['id_program'], $params);
			
			include_once("_views/admin/program.php");
		}
	}
	
	function ConfirmPayment() {
		if (isset($_SESSION['admin'])) {
			include_once("_models/dbchanges.php");
			
			confirmPayment("2", $_GET['id_program']);
			
			Base::view("admin", "ProgramSummary");
		}
	}
	
	function ExentPayment() {
		if (isset($_SESSION['admin'])) {
			include_once("_models/dbchanges.php");
			
			confirmPayment("3", $_GET['id_program']);
			
			Base::view("admin", "ProgramSummary");
		}
	}
	
	function ProgramDiet() {
		if (isset($_SESSION['admin'])) {
			include_once("_models/info.php");
			
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			Base::addScript("jquery.dd.min.js");
			Base::addScript("form.js");
			Base::addStyle("form.css");
			Base::addStyle("dd.css");
			Base::addStyle("program.css");
			Base::addStyle("jquery-ui-1.10.2.custom.min.css");
			
			loadProgramDietInfo(null, $_GET['id_program'], $params);
			
			include_once("_views/admin/diet.php");
		}
	}
	
	function ProgramActivity() {
		if (isset($_SESSION['admin'])) {
			include_once("_models/info.php");
			
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("jquery-ui-1.10.2.custom.min.js");
			Base::addScript("jquery.dd.min.js");
			Base::addScript("form.js");
			Base::addStyle("form.css");
			Base::addStyle("dd.css");
			Base::addStyle("program.css");
			
			loadProgramDietInfo(null, $_GET['id_program'], $params);
			
			include_once("_views/admin/activity.php");
		}
	}
}

?>
