<?php

class Modules {

	function __construct() {
		// DO NOTHING
	}

	function Index() {
		// DO NOTHING
	}
	
	function TopMenu() {
	
		if (isset($_SESSION['user'])) {
		
			require_once("includes/user.php");
			require_once("_models/info.php");
			
			$params['username'] = $_SESSION['user'];
			loadTopMenuInfo($_SESSION['user'], $params);
			
			
			require("_views/modules/top_menu.php");
			Base::addStyle("topmenu.css");
			Base::addScript("topmenu.js");
			
			
		}
	
	}
	
	function QuickUser() {
		if (!isset($_SESSION['user'])) {
			Base::addStyle("form.css");
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("login_form.js");
			require("_views/modules/module_quick_login.php");
		} else {
			
			require_once("includes/user.php");
			
			$current_user = new User($_SESSION['user']);
			$current_user->getInfo();
			
			$params['username'] = $current_user->info['username'];
			$params['messages'] = $current_user->info['username'];
			$params['avatar'] = "http://profile.ak.fbcdn.net/hprofile-ak-snc6/188083_224876510963142_1568890106_q.jpg";
			
			require("_views/modules/module_quick_user.php");
		}
	}
	
}

?>
