<?php

class Ajax {

	function __construct() {
		// DO NOTHING
	}

	function Index() {
		// DO NOTHING
	}
	
	function Read() {
		if (isset($_SESSION['user']) && isset($_GET['id_message'])) {
		
			require_once("includes/user.php");
			
			$current_user = new User($_SESSION['user']);
			$user_id = $current_user->getInfo("id_user");
			
			$info = new Connection();
			$messages = $info->nonQuery("update js_messages set recipent_read = 1 where id_message = :1 and id_recipent = :2", array($_GET['id_message'], $user_id));
		}
		die();
	}
	
	function Unread() {
		if (isset($_SESSION['user']) && isset($_GET['id_message'])) {
		
			require_once("includes/user.php");
			
			$current_user = new User($_SESSION['user']);
			$user_id = $current_user->getInfo("id_user");
			
			$info = new Connection();
			$messages = $info->nonQuery("update js_messages set recipent_read = 0 where id_message = :1 and id_recipent = :2", array($_GET['id_message'], $user_id));
		}
		die();
	}
	
	function Remove() {
		if (isset($_SESSION['user']) && isset($_GET['id_message'])) {
		
			require_once("includes/user.php");
			
			$current_user = new User($_SESSION['user']);
			$user_id = $current_user->getInfo("id_user");
			
			$info = new Connection();
			$messages = $info->nonQuery("update js_messages set deleted = 1 where id_message = :1 and id_recipent = :2", array($_GET['id_message'], $user_id));
		}
		die();
	}
	
	function SelfRemove() {
		if (isset($_SESSION['user']) && isset($_GET['id_message'])) {
		
			require_once("includes/user.php");
			
			$current_user = new User($_SESSION['user']);
			$user_id = $current_user->getInfo("id_user");
			
			$info = new Connection();
			$messages = $info->nonQuery("update js_messages set deleted = 1 where id_message = :1 and id_author = :2", array($_GET['id_message'], $user_id));
		}
		die();
	}
	
}

?>
