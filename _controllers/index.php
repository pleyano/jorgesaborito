<?php

class Index {

	function __construct() {
		// DO NOTHING
	}

	function Index() {
		
		include_once("_models/random.php");
		
		getWordpressPosts("localhost", "wordpress", "root", "toor", $params);
		
		require("_views/index.php");
		
	}
	
	function Header() {
		
		require("_views/header.php");
		
	}
	
	function Footer() {
		
		require("_views/footer.php");
		
	}

	function Login() {
		
		if (isset($_SESSION['user'])) {
			Base::view("index", "Index");
		} else if (isset($_POST['quick_submit']) && isset($_POST['quick_username']) && isset($_POST['quick_password'])) {
			require_once("includes/user.php");
			$login_user = new User($_POST['quick_username'], $_POST['quick_password']);
			if ($login_user->login() === true) {
				if (in_array("Administrador", $login_user->getRoles())) {
					$_SESSION['admin'] = "y";
				}
				Base::view("index", "Index");
			} else {
				$params['login_error'] = "El usuario o la contrase&ntilde;a introducidos son incorrectos";
				Base::addStyle("form.css");
				Base::addScript("jquery-1.9.1.min.js");
				Base::addScript("login_form.js");
				require("_views/login.php");
			}
			
		} else {
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("login_form.js");
			Base::addStyle("form.css");
			require("_views/login.php");
			
		}
		
	}
	
	function Register() {
	
		if (!isset($_SESSION['user'])) {
			if (isset($_POST['register_submit'])) {
				unset($_POST['register_submit']);
				unset($_POST['email2']);
				unset($_POST['password2']);
			
				require_once("_models/validation.php");
				
				if (validate_registration($_POST) === true) {
					include_once("_models/dbchanges.php");
					registerUser($_POST['username'], $_POST['password'], $_POST['email'], $_POST['birthday_day'], $_POST['birthday_month'], $_POST['birthday_year']);
					Base::view("index", "Index");
				} else {
					Base::addScript("jquery-1.9.1.min.js");
					Base::addScript("jquery.dd.min.js");
					Base::addScript("form.js");
					Base::addStyle("form.css");
					Base::addStyle("dd.css");
					require("_views/register.php");
				}
			} else {
				Base::addScript("jquery-1.9.1.min.js");
				Base::addScript("jquery.dd.min.js");
				Base::addScript("form.js");
				Base::addStyle("form.css");
				Base::addStyle("dd.css");
				require("_views/register.php");
			}
			
		} else {
		
			Base::addScript("jquery-1.9.1.min.js");
			Base::addScript("form.js");
			Base::addStyle("form.css");
			require("_views/register.php");
			
		}
	
	}
	
}

?>
