<?php

require_once("includes/gump.php");

class GUMP_extended extends GUMP{

	public function validate_max_val($field, $input, $param = NULL) {
		
		if(!isset($input[$field]))
		{
			return;
		}
		
		if((float)$input[$field] > (int)$param) 
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'	=> __FUNCTION__,
				'param' => $param				
			);
		}
	
	}

	public function validate_min_val($field, $input, $param = NULL) {
		
		if(!isset($input[$field]))
		{
			return;
		}
		if($input[$field] < (int)$param)
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'	=> __FUNCTION__,
				'param' => $param				
			);
		}
	
	}
	
	public function validate_username($field, $input, $param = NULL)
	{
		if(!isset($input[$field]))
		{
			return;
		}
		
		if(!preg_match("/^([a-zA-Z0-9ÁÉÍÓÚáéíóúÜüÇçÑñ_-])+$/i", $input[$field]) !== FALSE)
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'	=> __FUNCTION__,
				'param' => $param				
			);
		}
	}
	
	public function validate_image_ext($field, $input, $param = NULL)
	{
		if(!isset($input[$field]))
		{
			return;
		} 
		
		if(!preg_match("/^([a-zA-Z0-9ÁÉÍÓÚáéíóúÜüÇçÑñ._-])+((\.png)|(\.jpg)|(\.jpeg)|(\.gif))$/i", $input[$field]) !== FALSE)
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'	=> __FUNCTION__,
				'param' => $param				
			);
		}
	}
	
	public function validate_gender($field, $input, $param = NULL)
	{
		if(!isset($input[$field]))
		{
			return;
		}
		
		if(!preg_match("/^m|f$/i", $input[$field]) !== FALSE)
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'	=> __FUNCTION__,
				'param' => $param				
			);
		}
	}

}

?>