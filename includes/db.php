<?php
require_once("config.php");
/**
* Clase para la conexión con la base de datos
*
* @property
*/

class Connection extends Base {
	
	protected $connection;
	
	/** 
	* Constructor de la clase que conecta con la base de datos
	*/
	
	
	public function __construct() {
		$this->connect();
	}
	
	/** 
	* Conecta con la base de datos
	*/
	
	protected function connect() {
		
		$driver = $this->config("DATABASE", "driver");
		$server = $this->config("DATABASE", "server");
		$db = $this->config("DATABASE", "db");
		$user = $this->config("DATABASE", "user");
		$password = $this->config("DATABASE", "password");
		
		try {
			
			switch ($driver) {
				
				case "mysql":
				$this->connection = new PDO('mysql:host=' . $server . ';dbname=' . $db, $user, $password);
				break;
				
			}
			
		} catch (PDOException $e) {
			
			$this->connection = false;
			
		}
		
	}	
	
	/** 
	* Desconecta de la base de datos
	*/
	
	protected function disconnect() {
		
		$this->connection = NULL;
		
	}
	
	/**
	* Destructor de la clase que desconecta con la base de datos
	*/
	
	public function __destruct() {
		
		$this->disconnect();
		
	}
	
	/**
	* Realiza una consulta con los parámetros dados, que se sustituyen por :1, :2, :3...
	*
	* @param String $queryString Consulta de tipo SQL
	* @param Array $params Parametros que se sustituyen en la consulta
	*
	* @return array Conjunto de resultados
	*/
	
	public function query($queryString, $params = array()) {
		
		try {
			$query = $this->connection->prepare($queryString);
			$i = 1;
			foreach ($params as $p) {
			
				$query->bindValue(':' . $i, $p);
				$i++;
				
			}
			$query->execute();
			return $query->fetchAll();
			
			
		} catch (PDOException $e) {
			
			return NULL;
			
		}
		
	}
	
	
	/**
	* Realiza una consulta con los parámetros dados, que se sustituyen por :1, :2, :3...
	*
	* @param String $queryString Consulta de tipo SQL
	* @param Array $params Parametros que se sustituyen en la consulta
	*
	* @return boolean True si el resultado es satisfactorio
	*/
	
	public function nonQuery($queryString, $params = array()) {

		try {
			$query = $this->connection->prepare($queryString);
			$i = 1;
			foreach ($params as $p) {
			
				$query->bindValue(':' . $i, $p);
				$i++;
			}
			
			$query->execute();
			return true;
			
		} catch (PDOException $e) {
		
			return NULL;
			
		}
		
	}
	
}

?>
