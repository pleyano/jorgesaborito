<?php
defined("INDEX") or die();
ini_set("display_errors", 0);
session_start();


$params;
/**
* Clase básica del programa
*
*/

abstract class Base {
	
	static $body = "", $header = "";
	private static $scripts = array(), $styles = array();
	
	/** 
	* Retorna el valor de configuracion pasado por parametro
	*
	* @param string $section Sección del archivo
	* @param string $key Valor de configuracion
	* @return string
	*/
		
	static function config($section, $key) {

		$configuracion = (parse_ini_file('conf.ini', true));
		return $configuracion[$section][$key];	
		
	}
	
	/** 
	* Genera una vista
	*
	* @param string $controller Nombre de controlador
	* @param string $action Nombre de la acción
	*/
	
	static function view ($controller, $action) {
		require_once("_controllers/" . $controller . ".php");
		$instance = new $controller();
		$instance->$action();
	}
	
	static function addStyle($styleName) {
		if (!in_array($styleName, self::$styles))
			self::$styles[] = $styleName;
	
	}
	
	static function addScript($scriptName) {
		if (!in_array($scriptName, self::$scripts))
			self::$scripts[] = $scriptName;
	
	}
	
	static function render () {
		include_once("includes/default/default_header.php");
		echo "\n";
		
		foreach (self::$styles as $new_css) {
			if (is_file("css/" . $new_css)) 
				echo "<link rel='stylesheet' type='text/css' href='css/" . $new_css . "' />\n";
		}
		
		foreach (self::$scripts as $new_script) { 
			if (is_file("scripts/" . $new_script))
				echo "<script src='scripts/" . $new_script . "'></script>\n";
		}
		
		echo "</head>\n<body>\n";
		echo self::$body;
		include_once("includes/default/default_footer.php");
	
	}
		
}

?>
