<?php
/**
 * Clase User (hereda de Connection)
 */

require_once("db.php");

/**
*	Clase que controla las sesiones de usuario
*
*/

class User extends Connection {
	
	/** @var string $user Nombre de usuario */
	public $user;
	/**	@var string $password Contraseña (cifrada con el algoritmo MD5) */
	private $password = NULL;
	/**	@var array $info Array asociativo con información extraída de la base de datos. Inicialmente vacío. */
	public $info = array();
	
	/**
	*	Constructor de la clase: acepta usuario y contraseña
	*
	*	@param string $user Usuario
	*	@param string $password Contraseña. Acepta valor nulo
	*/
	
	public function __construct($user, $password = NULL) {
		
		$this->connect();
		$this->user = $user;
		if (!is_null($password)) $this->password = md5($password);
		
		
	}
	
	/**
	*
	* Autentica al usuario respecto a los datos de la base de datos. Además, carga su información en $info cuando exista el usuario
	*
	* @return boolean Devuelve true si el usuario y contraseña son correctos y false en cualquier otro caso
	*
	*/
	
	public function authenticate() {
	
		$result = $this->query("select * from js_users where username = :1 and password = :2", array($this->user, $this->password));
		if (sizeof($result)) {
			$this->getInfo();
			return true;	
		} else {
			return false;
		}
		
	}
	
	
	/**
	*
	* Autentica al usuario y  el nombre de usuario es almacenado como variable de sesión
	*
	* @return boolean Devuelve true si se pudo iniciar la sesión y false en caso contrario
	*
	*/
	
	public function login() {
	
		if ($this->authenticate() && $this->info['active'] == "0") {
			$_SESSION['user'] = $this->info['username'];
			$this->nonQuery("update js_users set last_connection = now() where username = :1", array($this->info['username']));
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	*
	* Desautentica al usuario
	*
	*
	*/
	
	public function logout() {
	
		unset($_SESSION['user']);
		unset($_SESSION['admin']);
		
	}
	
	/**
	*
	* Registra al usuario en la base de datos
	*
	* @return boolean Devuelve true si se pudo registrar y false en caso contrario
	*
	*/
	
	public function register($userData) {
		if (!$this->userExists()) {
			$result = $this->nonQuery("insert into js_users set username = :1, password = :2, email = :3, active = '0', regist_date = current_date", $userData);
			return $result;
		} else {
			return false;
		}
		
	}
	
	/**
	*
	* Comprueba si existe un nombre de usuario respecto a los datos de la base de datos
	*
	* @return boolean Devuelve true si el usuario existe y false en caso contrario
	*/
	
	public function userExists() {
	
		$result = $this->query("select 1 from js_users where username = :1", array($this->user));
		if (sizeof($result)) {
			return true;	
		} else {
			return false;
		}
		
	}
	
	/**
	*
	* Consulta la información de usuario almacenada en la base de datos y la almacena $info. La diferencia con authenticate es que no necesita contraseña.
	*
	* @param string $info Optional Campo que se quiere retornar
	*
	* @return boolean|string Devuelve true o la información solicitada si el usuario existe y false en caso contrario.
	*
	*/
	
	public function getInfo($info = NULL) {
	
		$result = $this->query("select * from js_users where username = :1", array($this->user));
		if (sizeof($result)) {
			$this->info = $result[0];
			if (!is_null($info) && isset($this->info[$info])) {
				return $this->info[$info];
			} else {
				return true;	
			}
		} else {
			return false;
		}
		
	}
	
	/**
	*
	* Almacena informacion en la base de datos
	*
	* @param  array $fields Array asociativo con los valores que se actualizaran
	*
	* @return boolean True si se actualizaron los datos y false si no
	*
	*/

	public function setInfo($fields = array()) {

		if (sizeof($fields)) {
			$params = array();
			$query = "update js_users set";
			$n_param = 1;
			foreach ($fields as $field=>$value) {
				if ($n_param == sizeof($fields)) {
					$query .= " " . $field . " = :" . $n_param;
				} else {
					$query .= " " . $field . " = :" . $n_param . ",";
				}
				$n_param++;
				$params[] = $value;
			}
			$params[] = $this->user;
			$query .= " where username = :" . $n_param;
			return $this->nonQuery($query, $params);
		} else {
			return false;
		}
	}

	/**
	*
	* Devuelve los roles que tiene asignado el usuario
	*
	* @return array 
	*/
	
	public function getRoles() {
	
		$result = $this->query("select js_roles.name from js_users join js_privileges on js_users.id_user = js_privileges.id_user join js_roles on js_privileges.id_role = js_roles.id_role where username = :1", array($this->user));
		
		$roles = array();
		
		foreach ($result as $role) {
			$roles[] = $role[0];
		}
		
		return $roles;
		
	}
	
	/**
	*
	* Asigna nuevos roles al usuario
	*
	* @param $role Nombre del rol que se asignará al usuario
	*
	* @return boolean Devuelve false si se produce algún error
	*/
	
	public function enRole($role) {
		
		$id_user = $this->query("select id_user from js_users where username = :1", array($this->user));
		$id_role = $this->query("select id_role from js_roles where name = :1", array($role));
		
		if (sizeof($id_user) && sizeof($id_role)) {
			
			$this->nonQuery("insert into js_privileges values ( :1 , :2 )", array($id_user[0][0], $id_role[0][0]));
			return true;
			
		} else {
			
			return false;
			
		}
		
	}
	
	/**
	*
	* Elimina roles del usuario
	*
	* @param $role Nombre del rol que se eliminará al usuario
	*
	* @return boolean Devuelve false si se produce algún error
	*/
	
	public function disRole($role) {
		
		$id_user = $this->query("select id_user from js_users where username = :1", array($this->user));
		$id_role = $this->query("select id_role from js_roles where name = :1", array($role));
		
		if (sizeof($id_user) && sizeof($id_role)) {
			
			$this->nonQuery("delete from js_privileges where id_user = :1 and id_role = :2 ", array($id_user[0][0], $id_role[0][0]));
			return true;
			
		} else {
			
			return false;
			
		}
		
	}
	
}


?>
