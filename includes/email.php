<?php
require_once("config.php");
require_once("Mail.php");

/**
* Clase para el envio de emails
*
*/

class Email extends Base {
	
	private $smtp_conf;
	
	/** 
	* Constructor de la clase que carga los datos del archivo conf.ini
	*/
	
	
	public function __construct() {
		
		$this->smtp_conf['host'] = $this->config("EMAIL", "server");
		$this->smtp_conf['port'] = $this->config("EMAIL", "port");
		$this->smtp_conf['username'] = $this->config("EMAIL", "user");
		$this->smtp_conf['password'] = $this->config("EMAIL", "password");
		$this->smtp_conf['auth'] = true;
		
	}
	
	/**
	* Envia un email de validación del usuario
	*
	* @param String $username Nombre de usuario al que se le envía el mensaje para validar el email
	*
	*/
	
	public function validateUser($username) {
	
		include_once("user.php");
		
		$validationText  = "Hola [username], \n\n ";
		$validationText .= "Has recibido este email como último paso para confirmar el registro de tu cuenta en [url].";
		$validationText .= " Si tú no te has registrado en [url], por favor, ignora este mensaje. \n\n";
		$validationText .= "--------------------------------\n";
		$validationText .= "INSTRUCCIONES PARA LA ACTIVACION\n";
		$validationText .= "--------------------------------\n\n";
		$validationText .= "Gracias por registrarte en [url]. Para asegurarnos que esta cuenta de correo es válida, necesitamos ";
		$validationText .= "que la actives pusando el siguiente enlace: [link]. Si tienes problemas con la validación, por favor ";
		$validationText .= "contacta con un administrador en [admin].\n\n";
		$validationText .= "(Algunos clientes de email no permiten los enlaces a webs externas. Copia y pega el enlace en ";
		$validationText .= "ese caso)\n\n";
		$validationText .= "Gracias por registrarte y disfruta de [url].\n\n";
		$validationText .= "[title]\n";
		
		
		$userToValidate = new User($username);
		$userToValidate->getInfo();
		
		if ($userToValidate->info['active'] != "0") {
			
			$validationText = str_replace("[url]", $this->config("SITE", "url"), $validationText);
			$validationText = str_replace("[link]", $this->config("SITE", "url") . "/validation.php?validate=" . $userToValidate->info['active'], $validationText);
			$validationText = str_replace("[admin]", $this->config("SITE", "admin"), $validationText);
			$validationText = str_replace("[title]", $this->config("SITE", "title"), $validationText);
			$validationText = str_replace("[username]", $username, $validationText);
		
			$smtp = Mail::factory('smtp', $this->smtp_conf);
			$headers = array (
					'From' => $this->config("SITE", "noresponse"), 
					'To' => $userToValidate->info['email'], 
					'Subject' => "Validacion de registro - jorgesaborito.localhost.com");
			
			$smtp->send($userToValidate->info['email'], $headers, $validationText);
			
		}
		
	}
}
?>
