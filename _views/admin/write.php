<?php defined("INDEX") or die(); 
Base::$body .= <<<'PHP_HTML_OUTPUT'


PHP_HTML_OUTPUT;

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Perfil de usuario</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("admin", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'
		<div class="user_functions">
			<h3>Enviar Mensaje</h3>
			<div id="messages">
				<form action="index.php?controller=admin&amp;action=Write" method="post">
				<div class="data_section">
					<label>Destinatario:</label>
					<input type="text" name="input_username" id="input_username" class="input_normal changing_form_input" value="
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['dest_username'])) ? $params['dest_username'] : '';

Base::$body .= <<<'PHP_HTML_OUTPUT'
" /> <a href="javascript:selectContact();" class="button_style" style="margin-left: 10px">Seleccionar</a>
				</div>
				<div class="data_section">
					<label>Asunto:</label>
					<input type="text" name="subject" class="input_full_size changing_form_input" />
				</div>
				<div class="data_section">
					<label>Mensaje:</label>
					<textarea id="message_box" name="content"></textarea>
				</div>
				<input name="send_message" type="submit" style="margin: 20px 0px 5px"  value="Enviar mensaje" />
				</form>
			</div>
		</div>
	</div>
</div> 
<script>
$("#message_box").wysiwyg({
	autoGrow:true, 
	controls: "bold,italic,underline,strikeThrough,|,justifyLeft,justifyCenter,justifyRight,justifyFull,|,insertOrderedList,insertUnorderedList,insertHorizontalRule",
	initialContent: ""
});
$("form").submit(function() {
	$("#message_box").wysiwyg("saveContent");
	return true;
})

function selectContact() {
	window.open( "index.php?controller=admin&action=SelectContact", "", "height=400, width=500, resizable=0");
}
</script>
PHP_HTML_OUTPUT;
?>