<?php defined("INDEX") or die(); 
Base::$body .= <<<'PHP_HTML_OUTPUT'

<div class="select_contact_container">
	<form action="index.php?controller=admin&amp;action=SelectContact" method="post">
		<input type="text" name="filter" class="value_default changing_form_input" value="
PHP_HTML_OUTPUT;

Base::$body .= (isset($params['filter'])) ?  $params['filter'] : "";

Base::$body .= <<<'PHP_HTML_OUTPUT'
" /> <input type="submit" value="Filtrar" />
	</form>
	<select class="input_normal" style="margin-top: 20px;" id="users" onChange="returnUsername()">
PHP_HTML_OUTPUT;

if (sizeof($params['users']) > 0) {
	Base::$body .= "<option value='" . $params['users'][0]['id_user'] . "'>Selecciona una opción...</option>\n";
} else {
	Base::$body .= "<option>No hay coincidencias con la búsqueda</option>\n";
}

foreach ($params['users'] as $user) {
	Base::$body .= "<option value='" . $user['username'] . "'>" . $user['username'] . "</option>\n";
}

Base::$body .= <<<'PHP_HTML_OUTPUT'

	</select>
	<br /><br /><br /><br /><br /><br />
	<a href="javascript:window.close();" class="button_style">Cerrar ventana</a>
</div>

<script>

$("#users").msDropDown();

function returnUsername() {

	window.opener.document.getElementById("input_username").value = document.getElementById("users").value;
	window.close();
	
}

</script>

PHP_HTML_OUTPUT;
?>