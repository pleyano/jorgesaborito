<?php defined("INDEX") or die(); 

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Panel de administración</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("admin", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'

		<div class="user_functions">

			<h3>Programa</h3>
			<ul class="user_program_sections">
				<li><a href="index.php?controller=admin&amp;action=ProgramSummary&amp;id_program=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_program'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">General</a></li>
PHP_HTML_OUTPUT;

if (!empty($params['program_summary']['diet'])) {
	Base::$body .= "<li><a href='index.php?controller=admin&amp;action=ProgramDiet&amp;id_program=" . $_GET['id_program'] . "'>Dieta</a></li>";
} else {
	Base::$body .= "<li><a class='disabled' href='javascript:;'>Dieta</a></li>";
}

if (!empty($params['program_summary']['activity'])) {
	Base::$body .= "<li><a href='index.php?controller=admin&amp;action=ProgramActivity&amp;id_program=" . $_GET['id_program'] ."'>Actividad física</a></li>";
} else {
	Base::$body .= "<li><a class='disabled' href='javascript:;'>Actividad física</a></li>";
}
Base::$body .= <<<'PHP_HTML_OUTPUT'
			</ul>
			
			<div class="user_function_content">
				<div class="user_section" stlye="overflow: auto">
					<h4 style="padding-top: 20px">Actividad física</h4>
					<div class="summary_buttons">
						Información no disponible
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

PHP_HTML_OUTPUT;
?>