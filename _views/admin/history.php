<?php defined("INDEX") or die(); 

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Panel de administración</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("admin", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'

		<div class="user_functions">

			<h3>
PHP_HTML_OUTPUT;

Base::$body .= $params['username'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
</h3>
			<ul class="admin_user_sections">
				<li><a href="index.php?controller=admin&amp;action=User&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Información general</a></li>
				<li><a href="index.php?controller=admin&amp;action=History&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Historial</a></li>
				<li><a href="index.php?controller=admin&amp;action=Programs&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Programas</a></li>
				<li><a href="index.php?controller=admin&amp;action=Actions&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Acciones</a></li>
			</ul>
			<div class="user_function_content">
				<h4>Historial</h4>
			<table class="table_history table_users">
			<tr><th>Fecha</th><th>Peso</th><th>Altura</th><th>IMC</th><th>Actividad física</th><th>Sensación de hambre</th><th>Cont. brazo</th><th>Cont. pierna</th><th>Cont. cadera</th><th>Cont. abdomen</th><th>PTG</th></tr>
PHP_HTML_OUTPUT;

foreach ($params['history'] as $row) {
	
	Base::$body .= "<tr><td>" . $row['date'] . "</td><td>" . $row['weight'] . "</td><td>" . $row['height'] . "</td><td>" . $row['imc'] . "</td><td><img src='images/activity_" . $row['activity'] . ".png' alt='' /></td><td><img src='images/hunger_" . $row['hunger'] . ".png' alt='' /></td><td>" . $row['arm'] . "</td><td>" . $row['leg'] . "</td><td>" . $row['hips'] . "</td><td>" . $row['waist'] . "</td><td>" . $row['ptg'] . "</td></tr>";

}

Base::$body .= <<<'PHP_HTML_OUTPUT'
			</table>
			</div>
		</div>
	</div>
</div>

PHP_HTML_OUTPUT;
?>