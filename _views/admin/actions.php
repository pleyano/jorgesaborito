<?php defined("INDEX") or die(); 

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Panel de administración</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("admin", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'

		<div class="user_functions">

			<h3>
PHP_HTML_OUTPUT;

Base::$body .= $params['username'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
</h3>
			<ul class="admin_user_sections">
				<li><a href="index.php?controller=admin&amp;action=User&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Información general</a></li>
				<li><a href="index.php?controller=admin&amp;action=History&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Historial</a></li>
				<li><a href="index.php?controller=admin&amp;action=Programs&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Programas</a></li>
				<li><a href="index.php?controller=admin&amp;action=Actions&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Acciones</a></li>
			</ul>
			<div class="user_function_content" style="text-align: center">
				<h4>Acciones</h4>
				<div class="admin_actions">
					<div class="action_button"><a href="index.php?controller=admin&amp;action=Write&amp;id_dest=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Enviar Mensaje</a></div>
					<div class="action_button"><a href="index.php?controller=admin&amp;action=ActDeact&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">
PHP_HTML_OUTPUT;

Base::$body .= ($params['active'] == 0) ? "Bloquear" : "Desbloquear";

Base::$body .= <<<'PHP_HTML_OUTPUT'
 Usuario</a></div>
					<div class="action_button"><a href="index.php?controller=admin&amp;action=DeleteUsert&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Eliminar Usuario</a></div

				</div>
			</div>
		</div>
	</div>
</div>

PHP_HTML_OUTPUT;
?>