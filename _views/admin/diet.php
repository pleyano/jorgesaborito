<?php defined("INDEX") or die(); 

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Panel de administración</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("admin", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'

		<div class="user_functions">

			<h3>Programa</h3>
			<ul class="user_program_sections">
				<li><a href="index.php?controller=admin&amp;action=ProgramSummary&amp;id_program=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_program'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">General</a></li>
PHP_HTML_OUTPUT;

if (!empty($params['program_summary']['diet'])) {
	Base::$body .= "<li><a href='index.php?controller=admin&amp;action=ProgramDiet&amp;id_program=" . $_GET['id_program'] . "'>Dieta</a></li>";
} else {
	Base::$body .= "<li><a class='disabled' href='javascript:;'>Dieta</a></li>";
}

if (!empty($params['program_summary']['activity'])) {
	Base::$body .= "<li><a href='index.php?controller=admin&amp;action=ProgramActivity&amp;id_program=" . $_GET['id_program'] ."'>Actividad física</a></li>";
} else {
	Base::$body .= "<li><a class='disabled' href='javascript:;'>Actividad física</a></li>";
}
Base::$body .= <<<'PHP_HTML_OUTPUT'
			</ul>
			
			<div class="user_function_content">
				<div class="user_section" stlye="overflow: auto">
					<h4 style="padding-top: 20px">Dieta</h4>
					<div class="summary_buttons">
						<label>Ver: </label>
						<select style="text-align: left;" class="input_normal">
							<option value="1">Todo</option>
							<option value="X">Ayuno</option>
							<option value="A">Pan, cereal, pasta, arroz</option>
							<option value="B">Pescado, carne, legumbres</option>
							<option value="C">Frutas y verduras</option>
							<option value="D">Leche, huevos y lácteos</option>
						</select>
					</div>
					<table class="table_dishes">
					<tr>
						<td></td><td class="dish_week_day">Lunes</td><td class="dish_week_day">Martes</td><td class="dish_week_day">Miércoles</td><td class="dish_week_day">Jueves</td><td class="dish_week_day">Viernes</td><td class="dish_week_day">Sábado</td><td class="dish_week_day">Domingo</td>
					</tr>
PHP_HTML_OUTPUT;

$comidas = array("Desayuno", "Media Mañana", "Almuerzo", "Merienda", "Cena", "Recena");

for ($i = 0; $i < 7; $i++) {
	$params['program_diet']['day' . ($i + 1)] = explode("|", $params['program_diet']['day' . ($i + 1)]);
}

for ($i = 0; $i < 6; $i++) {


Base::$body .= "<tr>";
Base::$body .= '<td class="dish_time">' . $comidas[$i] . '</td>';

	for ($j = 0; $j < 7; $j++) {
		
Base::$body .= <<<'PHP_HTML_OUTPUT'
<td class="dish_cell">
					<div class="dish admin_dish_
PHP_HTML_OUTPUT;
Base::$body .= $params['program_diet']['day' . ($j + 1)][$i];
Base::$body .= <<<'PHP_HTML_OUTPUT'
"></div>
				</td>
PHP_HTML_OUTPUT;
	
	}
	Base::$body .= '</tr>';	
}
	
Base::$body .= <<<'PHP_HTML_OUTPUT'
				</table>
				<h4 style="padding-top: 20px">Test 24 hours</h4>
				
								
				<div id="tabs">
					<ul>
						<li><a href="#tabs-1">Lunes</a></li>
						<li><a href="#tabs-2">Martes</a></li>
						<li><a href="#tabs-3">Miércoles</a></li>
						<li><a href="#tabs-4">Jueves</a></li>
						<li><a href="#tabs-5">Viernes</a></li>
						<li><a href="#tabs-6">Sábado</a></li>
						<li><a href="#tabs-7">Domingo</a></li>
					</ul>
					<div id="tabs-1">
						<p>
PHP_HTML_OUTPUT;
Base::$body .= (!empty($params['program_diet']['24hours1']) ? $params['program_diet']['24hours1'] : "No hay información");
Base::$body .= <<<'PHP_HTML_OUTPUT'
</p>
					</div>
					<div id="tabs-2">
						<p>
PHP_HTML_OUTPUT;
Base::$body .= (!empty($params['program_diet']['24hours2']) ? $params['program_diet']['24hours2'] : "No hay información");
Base::$body .= <<<'PHP_HTML_OUTPUT'
</p>
					</div>
					<div id="tabs-3">
						<p>
PHP_HTML_OUTPUT;
Base::$body .= (!empty($params['program_diet']['24hours3']) ? $params['program_diet']['24hours3'] : "No hay información");
Base::$body .= <<<'PHP_HTML_OUTPUT'
</p>
					</div>
					<div id="tabs-4">
						<p>
PHP_HTML_OUTPUT;
Base::$body .= (!empty($params['program_diet']['24hours4']) ? $params['program_diet']['24hours4'] : "No hay información");
Base::$body .= <<<'PHP_HTML_OUTPUT'
</p>
					</div>
					<div id="tabs-5">
						<p>
PHP_HTML_OUTPUT;
Base::$body .= (!empty($params['program_diet']['24hours5']) ? $params['program_diet']['24hours5'] : "No hay información");
Base::$body .= <<<'PHP_HTML_OUTPUT'
</p>
					</div>
					<div id="tabs-6">
						<p>
PHP_HTML_OUTPUT;
Base::$body .= (!empty($params['program_diet']['24hours6']) ? $params['program_diet']['24hours6'] : "No hay información");
Base::$body .= <<<'PHP_HTML_OUTPUT'
</p>
					</div>
					<div id="tabs-7">
						<p>
PHP_HTML_OUTPUT;
Base::$body .= (!empty($params['program_diet']['24hours7']) ? $params['program_diet']['24hours7'] : "No hay información");
Base::$body .= <<<'PHP_HTML_OUTPUT'
</p>
					</div>
				</div>
				
				
				
				<h4 style="padding-top: 20px">Observaciones</h4>
				<textarea disabled="disabled" name="observations" class="textarea_more_info changing_form_input value_default input_full_size" style="height: 200px; -moz-box-sizing: border-box; box-sizing: border-box;">
PHP_HTML_OUTPUT;
	

Base::$body .= $params['program_diet']['observations'];	

Base::$body .= <<<'PHP_HTML_OUTPUT'
</textarea>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(".summary_buttons select").msDropDown(); 
	$( "#tabs" ).tabs();
	$(".summary_buttons select").on('change', function() {
		var selector = "";
		switch ($(this).val()) {
			case "1":
				selector = "*";
				break;
			default:
				selector = ".admin_dish_" + $(this).val();
				break;
		}
		
		$(".dish_cell div").animate({opacity: 0.3});
		$(".dish_cell " + selector).animate({opacity: 1});
		
	});
	
</script>
PHP_HTML_OUTPUT;
?>