<?php defined("INDEX") or die(); 

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Panel de administración</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("admin", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'

		<div class="user_functions">

			<h3>
PHP_HTML_OUTPUT;

Base::$body .= $params['username'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
</h3>
			<ul class="admin_user_sections">
				<li><a href="index.php?controller=admin&amp;action=User&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Información general</a></li>
				<li><a href="index.php?controller=admin&amp;action=History&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Historial</a></li>
				<li><a href="index.php?controller=admin&amp;action=Programs&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Programas</a></li>
				<li><a href="index.php?controller=admin&amp;action=Actions&amp;id_user=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_user'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">Acciones</a></li>
			</ul>
			<div class="user_function_content">
				<div class="user_section" stlye="overflow: auto">
					<h4>Datos personales</h4>
					<span class="profile_summary_title" style="line-height: 25px;">Nombre:</span> 
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['name'])) ? $params['name'] : "No hay información";

Base::$body .= <<<'PHP_HTML_OUTPUT'

					<br />
					<span class="profile_summary_title" style="line-height: 25px;">Fecha de nacimiento:</span> 
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['birthday'])) ? $params['birthday'] : "No hay información";

Base::$body .= <<<'PHP_HTML_OUTPUT'

					<br />
					<span class="profile_summary_title" style="line-height: 25px;">Email:</span> 
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['email'])) ? $params['email'] : "No hay información";

Base::$body .= <<<'PHP_HTML_OUTPUT'

					<br />
					<span class="profile_summary_title" style="line-height: 25px;">Fecha de registro:</span> 
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['regist_date'])) ? $params['regist_date'] : "No hay información";

Base::$body .= <<<'PHP_HTML_OUTPUT'

					<br />
					<span class="profile_summary_title" style="line-height: 25px;">Genero:</span> 
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['gender'])) ? ($params['gender'] == "M" ? "Hombre" : "Mujer") : "No hay información";

Base::$body .= <<<'PHP_HTML_OUTPUT'

					<br />
					<span class="profile_summary_title" style="line-height: 25px;">Opinión:</span>  
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['evaluation'])) ? $params['evaluation'] : "No hay información";

Base::$body .= <<<'PHP_HTML_OUTPUT'
				
					<h4 style="padding-top: 20px">Enfermedades</h4>
					<p>
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['illnesses'])) ? $params['illnesses'] : "No hay información";

Base::$body .= <<<'PHP_HTML_OUTPUT'
</p>
				</div>
				<div class="user_section">
					<h4>Alergias</h4>
					<p>
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['allergies'])) ? $params['allergies'] : "No hay información";

Base::$body .= <<<'PHP_HTML_OUTPUT'
</p>
				</div>
				<div class="user_section">
					<h4>Otros</h4>
					<p>
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['other'])) ? $params['other'] : "No hay información";

Base::$body .= <<<'PHP_HTML_OUTPUT'
</p>
				</div>
			</div>
		</div>
	</div>
</div>

PHP_HTML_OUTPUT;
?>