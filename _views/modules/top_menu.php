<?php defined("INDEX") or die(); 
Base::$body .= <<<'PHP_HTML_OUTPUT'

<div class="topmenu">
	<a href="index.php?controller=profile&amp;action=Profile" id="topmenu_profile"><img src="images/icons/user.png" alt="Perfil"></a>
	
PHP_HTML_OUTPUT;
if (isset($_SESSION['admin'])) {
	Base::$body .= '<a href="index.php?controller=admin&amp;action=Index" id="topmenu_admin"><img src="images/icons/gear.png" alt="Administrador" /></a>';
}
Base::$body .= <<<'PHP_HTML_OUTPUT'

	<a href="index.php?controller=profile&amp;action=Inbox" id="topmenu_messages"><img src="images/icons/speech.png" alt="Mensajes">
	
PHP_HTML_OUTPUT;
Base::$body .= '<span id="top_menu_unread_messages" class="num_messages"';
if (!$params['unread_messages']) {
	Base::$body .= ' style="display: none"';
}
Base::$body .= '>' .  $params['unread_messages'] . '</span>';
Base::$body .= <<<'PHP_HTML_OUTPUT'

	</a>
	<a href="index.php?controller=profile&amp;action=Logout" id="topmenu_logout"><img src="images/icons/outgoing.png" alt="Logout"></a>
</div>

PHP_HTML_OUTPUT;


?>