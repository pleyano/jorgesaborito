<?php defined("INDEX") or die(); 
Base::$body .= <<<'PHP_HTML_OUTPUT'

<div class="user_features">
	<ul>
		<li><a href="#" class="feature_title">Inicio</a></li>
		<li><a href="index.php?controller=admin&amp;action=Index">Resumen</a></li>
		<li><a href="index.php?controller=admin&amp;action=Data">Configuración del sitio</a></li>
	</ul>
	<ul>
		<li><a href="#" class="feature_title">Usuarios</a></li>
		<li><a href="index.php?controller=admin&amp;action=Users">Usuarios</a></li>
		<li><a href="index.php?controller=admin&amp;action=Programs">Programas</a></li>
	</ul>
	<ul>
		<li><a href="#" class="feature_title">Mensajes privados</a></li>
		<li><a href="index.php?controller=admin&amp;action=Write">Redactar mensaje</a></li>
		<li><a href="index.php?controller=admin&amp;action=Inbox">Bandeja de entrada</a></li>
		<li><a href="index.php?controller=admin&amp;action=Outbox">Bandeja de salida</a></li>
	</ul>
</div>

PHP_HTML_OUTPUT;

?>