<?php defined("INDEX") or die(); 
Base::$body .= <<<'PHP_HTML_OUTPUT'

<div class="user_features">
	<ul>
		<li><a href="#" class="feature_title">Perfil de usuario</a></li>
		<li><a href="index.php?controller=profile&amp;action=Index">Inicio</a></li>
		<li><a href="index.php?controller=profile&amp;action=Data">Mi ficha de datos</a></li>
		<li><a href="index.php?controller=profile&amp;action=Progress">Mi progreso</a></li>
		<li><a href="index.php?controller=profile&amp;action=Preferences">Preferencias</a></li>
	</ul>
	<ul>
		<li><a href="#" class="feature_title">Programas</a></li>
		<li><a href="index.php?controller=profile&amp;action=Programs">Mis programas</a></li>
		<li><a href="index.php?controller=profile&amp;action=Newprogram">Nuevo programa</a></li>
	</ul>
	<ul>
		<li><a href="#" class="feature_title">Mensajes privados</a></li>
		<li><a href="index.php?controller=profile&amp;action=Write">Redactar mensaje</a></li>
		<li><a href="index.php?controller=profile&amp;action=Inbox">Bandeja de entrada</a></li>
		<li><a href="index.php?controller=profile&amp;action=Outbox">Bandeja de salida</a></li>
	</ul>
</div>

PHP_HTML_OUTPUT;


?>