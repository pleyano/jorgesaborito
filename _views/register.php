<?php defined("INDEX") or die(); 

Base::view("index", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'



<div class="content">
<div class="wrap">
<h2>Formulario de registro</h2>
<div class="main" style="margin: 0 auto; float: none;">
<form action="index.php?controller=index&amp;action=Register" method="post" id="registration_form">
	<div class="data_left_column">
		<div class="data_section">
			<label>Nombre de usuario: </label><input class="data_section_input changing_form_input input_normal" value="" type="text" name="username" />
		</div>
		
		<div class="data_section">
			<label>Contraseña: </label><input class="data_section_input changing_form_input input_normal" value="" type="text" name="password" />
		</div>
		
		<div class="data_section">
			<label>Confirmar contraseña: </label><input class="data_section_input changing_form_input input_normal" value="" type="text" name="password2" />
		</div>
	</div>
	<div class="data_right_column">
	<div class="data_section">
		<label>Email: </label><input class="data_section_input changing_form_input input_normal" value="" type="text" name="email" />
	</div>
	<div class="data_section">
		<label>Confirmar email: </label><input class="data_section_input changing_form_input input_normal" value="" type="text" name="email2" />
	</div>
	<div class="data_section">
		<label>Fecha de nacimiento: </label>
		<select class="input_smaller" name="birthday_day">
			
PHP_HTML_OUTPUT;
	
	for ($i = 1; $i <= 31; $i++) {
		Base::$body .= '<option value="' . $i . '">' . $i . '</option>';
	}
	
Base::$body .= <<<'PHP_HTML_OUTPUT'
		</select>
		<select class="input_small" name="birthday_month">
			<option value="1">Enero</option>
			<option value="2">Febrero</option>
			<option value="3">Marzo</option>
			<option value="4">Abril</option>
			<option value="5">Mayo</option>
			<option value="6">Junio</option>
			<option value="7">Julio</option>
			<option value="8">Agosto</option>
			<option value="9">Septiembre</option>
			<option value="10">Octubre</option>
			<option value="11">Noviembre</option>
			<option value="12">Diciembre</option>
		</select>
		<select class="input_smaller" name="birthday_year">
			
PHP_HTML_OUTPUT;
	
	for ($i = (int)date("Y")-10; $i > (int)date("Y")-90; $i--) {
		Base::$body .= '<option value="' . $i . '">' . $i . '</option>';
	}
	
Base::$body .= <<<'PHP_HTML_OUTPUT'
		</select>
	</div>
	</div>
	<div class="data_section" style="clear: both;">
		<label>Términos de uso: </label>
		<textarea class="textarea_full_size terms_of_use">
AVISO LEGAL

	I– OBJETO
Este sitio web proporciona información sobre los servicios y actividades de:
Compraunaweb s.l.
Teléfono "tu elefono"
Correo "tu correo"
Titular "tu nombre"
 
	II– PROPIEDAD INTELECTUAL
Los contenidos de esta web, incluyendo, entre otros, imágenes, logotipos, gráficos, animaciones, textos o aplicaciones informáticas, cualesquiera que sea su formato, lenguaje de programación y forma de representación, así como el nombre de dominio www.compraunaweb.com son propiedad del Titular, o bien ha adquirido los correspondientes derechos de sus propietarios, o tiene el consentimiento de éstos para su publicación, estando protegidos por las leyes y tratados internacionales en materia de propiedad intelectual y, en su caso, industrial, así como por la normativa reguladora de los nombres de dominio.
 
Se permite el uso de hipervínculos a esta web y la utilización o reproducción, total o parcial, sin ánimo de lucro, de sus contenidos siempre que se haga constar la fuente y se incluya un hipervínculo a esta web, y en los casos permitidos en los artículos 31 a 34 de la Ley de Propiedad Intelectual (Real Decreto Legislativo 1/1996, de 12 de abril). Queda prohibido el uso de marcos (frames) o cuales quiera otros mecanismos destinados a ocultar el origen o fuente de los contenidos, cualquier uso, transformación o explotación de los mismos con finalidades comerciales, promocionales o contrarias a la ley, moral u orden público, que pudiera lesionar los intereses o perjudicar la imagen del Titular o de terceros legítimos, que constituyan competencia desleal o, en general, en contra de lo dispuesto en estas Condiciones de Uso.
 
	III– PRIVACIDAD Y PROTECCIÓN DE DATOS
Al navegar por estas páginas, de forma automática Ud. facilita al servidor de la web información relativa a su dirección IP (identificador único para la transmisión de paquetes entre ordenadores conectados a Internet), fecha y hora de acceso, el hipervínculo que le ha reenviado a éstas, su sistema operativo y el navegador utilizado. Sin perjuicio de que la Agencia Española de Protección de Datos (en adelante, APD) considera la IP como dato de carácter personal, el Titular no puede obtener por si solo, ni tiene intención de hacerlo a menos que se le cause algún perjuicio, información alguna acerca del titular de la conexión a Internet a la que corresponde. Estos datos serán guardados y utilizados únicamente para el control y realización de estadísticas de acceso y visitas a la web, y en ningún caso serán comunicados o cedidos a terceros.
 
Este sitio web no utiliza cookies ni cualquier otro dispositivo análogo que permita la obtención de datos personales sin su consentimiento.
 
En caso de que Ud. establezca algún tipo de comunicación con el Titular, ya sea por teléfono, fax, correo ordinario o electrónico, sus datos serán guardados e incluidos en un fichero propiedad del Titular, con la finalidad de responder a su consulta, sugerencia o queja, informar o prestar el servicio solicitado. Fuera de los casos legalmente establecidos, sus datos no serán comunicados ni cedidos a terceros sin su consentimiento expreso. Dicho fichero se encuentra convenientemente inscrito en el Registro General de la APD y sobre él se aplican las medidas técnicas, legales y organizativas establecidas en la normativa sobre protección de datos de carácter personal para evitar cualquier acceso, alteración, pérdida o tratamiento no autorizados.
 
Tiene Ud. derecho a acceder a dicho fichero para consultar sus datos, rectificarlos o cancelarlos, así como a oponerse a todo tratamiento respecto de los mismos, con las excepciones y límites establecidos en la legislación aplicable. En caso de que desee ejercer dichos derechos, dirija al Titular una solicitud firmada, acompañando fotocopia de su D.N.I. o documento equivalente. En el sitio web de la APD tiene a su disposición modelos de formularios y más información al respecto.
 
	IV– LIMITACIÓN DE RESPONSABILIDAD
Los contenidos de este sitio web se ofrecen con una finalidad informativa y divulgativa por lo que el Titular se exime de cualquier responsabilidad, directa o indirecta, que pudiera derivarse del uso o aplicación de la información de esta web fuera de sus propósitos.
 
Los enlaces (hipervínculos) o contenidos de terceros que aparecen en esta web se facilitan con la finalidad de ampliar la información o indicar otro punto de vista. Su inclusión no implica la aceptación de dichos contenidos, ni la asociación del Titular con los responsables de dichas páginas web, por lo que rechaza toda responsabilidad en relación con los mismos, así como por los daños que pudieran causarse por cualquier motivo en su sistema informático (equipo y aplicaciones), documentos o ficheros. El Titular sólo podrá ser responsable por dichos contenidos conforme a lo establecido en la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico, en caso de que, habiendo tenido conocimiento efectivo de su ilicitud o de que lesiona los bienes o intereses de un tercero, no suprima o inutilice el enlace a los mismos.
 
El Titular no garantiza la fiabilidad, disponibilidad o continuidad de este sitio web ni de sus contenidos por motivos técnicos, de seguridad, control o mantenimiento del servicio, por fallos debidos al servidor que aloja los contenidos o de otros intermediarios o proveedores, por ataques contra el sistema informático, ni por cualesquiera otros motivos que se deriven de causas que escapen a su control, por lo que se exime de cualquier responsabilidad, directa o indirecta, por los mismos.
 
El Titular no se hace responsable de los fallos, errores o daños, directos o indirectos, que puedan causarse al sistema informático del usuario o a los ficheros o documentos almacenados en el mismo, que sean causados o se deriven de la capacidad o calidad de su sistema informático o de la presencia de un virus o cualquier otra aplicación informática dañina en el ordenador que sea utilizado para la conexión a los contenidos de la web, de la calidad de su conexión o acceso a Internet, por un mal funcionamiento de su navegador, o por el uso de aplicaciones informáticas cuyas versiones no estén actualizadas o no se obtenga la correspondiente licencia de usuario.
 
	V– MODIFICACIONES Y ACTUALIZACIONES
El Titular se reserva la facultad de efectuar, en cualquier momento y sin necesidad de previo aviso, modificaciones y actualizaciones de la información contenida en la web, de la configuración, disponibilidad y presentación de ésta, así como de las presentes Condiciones de Uso.
 
	VI– LEY APLICABLE Y FORO
Las cuestiones relacionadas con el uso de esta web o sus contenidos se regirán y serán interpretadas conforme a las presentes Condiciones de Uso y la legislación española, sometiéndose las partes, salvo en los casos en que no esté legalmente permitido, a la jurisdicción de los Juzgados y Tribunales de la ciudad de Granada (España) para la resolución de los conflictos concernientes al uso de esta web, con renuncia de forma expresa a cualquier otro foro que pudiera corresponderles.
 
Murcia, 1 de octubre de 2012.
 
Información facilitada en cumplimiento con lo dispuesto en el artículo 10 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico.
 
Este texto está adaptado por el Titular a partir de un aviso legal genérico por un abogado especialista en tecnologías de la información.</textarea>
	</div>
	<br />
    <input type="submit" name="register_submit" value="Enviar" />

</form>
</div>
</div>
</div>
<script>
 $("select").msDropDown();
</script>
PHP_HTML_OUTPUT;

Base::view("index", "Footer");
?>