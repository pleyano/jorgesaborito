<?php defined("INDEX") or die(); 
Base::$body .= <<<'PHP_HTML_OUTPUT'


PHP_HTML_OUTPUT;

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Perfil de usuario</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("profile", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'

		<div class="user_functions">

			<h3>Inicio</h3>
			<div class="user_function_content">
				<h4>Resumen</h4>
				<div class="user_section" style="overflow: auto;">
					<div class="profile_summary_column1">
					<span class="profile_summary_title">Mi nombre:</span> 
PHP_HTML_OUTPUT;
Base::$body .= $params['profile_name'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
					
					<br />
					<span class="profile_summary_title">Mi peso:</span> 
PHP_HTML_OUTPUT;
Base::$body .= $params['profile_weight'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
 kg.
					<br />
					<span class="profile_summary_title">Mi altura:</span> 
PHP_HTML_OUTPUT;
Base::$body .= $params['profile_height'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
 cm.
					<br />
					</div>
					<div class="profile_summary_column2">
					<span class="profile_summary_title">Evaluacion del experto:</span> 
PHP_HTML_OUTPUT;
Base::$body .= $params['profile_evaluation'];
Base::$body .= <<<'PHP_HTML_OUTPUT'

					</div>
				</div>
				<h4>Mi progreso</h4>
				<div class="user_section">
					<img src="images/stat1.png" alt="" />
				</div>
				<h4>Ultimos mensajes</h4>
				<div class="user_section">
					<div class="profile_message_sumary_block">
					@ <span class="profile_message_summary_from">Pleyano</span> - 17/04/2013 - <span class="profile_message_summary">Lorem ipsum dolor sit amet comit lalal adofosifetu reqte lalam salam malicum</span>
					</div>
					<div class="profile_message_summary_block">
					@ <span class="profile_message_summary_from">Pleyano</span> - 17/04/2013 - <span class="profile_message_summary">Lorem ipsum dolor sit amet comit lalal adofosifetu reqte lalam salam malicum</span>
					</div>
					<div class="profile_message_summary_block">
					@ <span class="profile_message_summary_from">Pleyano</span> - 17/04/2013 - <span class="profile_message_summary">Lorem ipsum dolor sit amet comit lalal adofosifetu reqte lalam salam malicum</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

PHP_HTML_OUTPUT;
?>