<?php defined("INDEX") or die(); 
Base::$body .= <<<'PHP_HTML_OUTPUT'


PHP_HTML_OUTPUT;

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Perfil de usuario</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("profile", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'

		<div class="user_functions" style="overflow: hidden;">

			<h3>Nueva dieta</h3>
			<div class="program_section">
				
PHP_HTML_OUTPUT;

Base::$body .= '<form action="' . $params['form_action'] . '" method="post">';

Base::$body .= <<<'PHP_HTML_OUTPUT'

					<h4>TEST 1 - Grupos de alimentos</h4>
					<p>Recoge durante una semana, tus comidas mas frecuentes. </p>
					<table class="table_dishes">
					<tr>
						<td></td><td class="dish_week_day">Lunes</td><td class="dish_week_day">Martes</td><td class="dish_week_day">Miércoles</td><td class="dish_week_day">Jueves</td><td class="dish_week_day">Viernes</td><td class="dish_week_day">Sábado</td><td class="dish_week_day">Domingo</td>
					</tr>
PHP_HTML_OUTPUT;

$comidas = array("Desayuno", "Media Mañana", "Almuerzo", "Merienda", "Cena", "Recena");

for ($i = 0; $i < 6; $i++) {
Base::$body .= "<tr>";
Base::$body .= '<td class="dish_time">' . $comidas[$i] . '</td>';

	for ($j = 0; $j < 7; $j++) {
	
Base::$body .= <<<'PHP_HTML_OUTPUT'
<td>
					<div class="dish">
						
PHP_HTML_OUTPUT;
Base::$body .= '<input type="text" value="" name="day' . $j. "time" . $i . '" />';
Base::$body .= <<<'PHP_HTML_OUTPUT'

						<span class="dish_preview"></span>
						<div class="dish_options">
							<div class="dish_options_section_A" title="Pan, cereal, pasta, arroz">A</div>
							<div class="dish_options_section_B" title="Pescado, carne, legumbres">B</div>
							<div class="dish_options_section_C" title="Frutas y verduras">C</div>
							<div class="dish_options_section_D" title="Leche, huevos y lácteos">D</div>
							<div class="dish_options_section_X">X</div>
						</div>
					</div>
				</td>
PHP_HTML_OUTPUT;
	
	}
	Base::$body .= '</tr>';	
}
	
Base::$body .= <<<'PHP_HTML_OUTPUT'
				</table>
					<script>
						$("div").tooltip({
							track: true,
							show: false,
							hide: false,
							tooltipClass: "dish_tooltip",
							position: { 
								at: 'center bottom', 
								my: 'center top+15'
							}})
						$(".dish_options").hide();
						$(".dish").hover(function() {
							$(this).css("z-index", "1").children("div").fadeIn(200)
						}, function() {
							$(this).css("z-index", "0").children("div").stop(true).hide()
						});
						$(".dish_options div").click(function() { 
							if ($(this).text() != "X") {
								$(this).parent().parent().css("background-image", $(this).css("background-image")); 
								$(".dish_options").hide(); 
								$(this).parent().parent().children("input").val($(this).text())
							} else {
								$(this).parent().parent().css("background-image", "url(css/images/test1X.png)");
								$(".dish_options").hide(); 
								$(this).parent().parent().children("input").val("");
							}
						});
					</script>
					
					<h4>TEST 2 - Last 24 hours</h4>
					<p>Escribe todo, todo lo que comas en un día. Desde que te has  levantado hasta justo antes de dormir. Repite el  test en cuatro ocasiones y  días no consecutivos.</p>
					

					<div class="hours_day">
						<label>Lunes:</label> <textarea name="24hours_day1" class="food_preview"></textarea>
					</div>
					<div class="hours_day">
						<label>Martes:</label> <textarea name="24hours_day2" class="food_preview"></textarea>
					</div>
					<div class="hours_day">
						<label>Miércoles:</label> <textarea name="24hours_day3" class="food_preview"></textarea>
					</div>
					<div class="hours_day">
						<label>Jueves:</label> <textarea name="24hours_day4" class="food_preview"></textarea>
					</div>
					<div class="hours_day">
						<label>Viernes:</label> <textarea name="24hours_day5" class="food_preview"></textarea>
					</div>
					<div class="hours_day">
						<label>Sábado:</label> <textarea name="24hours_day6" class="food_preview"></textarea>
					</div>
					<div class="hours_day">
						<label>Domingo:</label> <textarea name="24hours_day7" class="food_preview"></textarea>
					</div>
					
					<h4 style="padding-top: 30px">Observaciones</h4>
					<textarea class="textarea_more_info changing_form_input value_default input_full_size" style="height: 200px;" name="observations"></textarea>
					<input type="submit" value="Siguiente" name="form_diet_submit" />
				</form>
			</div>
		</div>
	</div>
</div>

PHP_HTML_OUTPUT;
?>