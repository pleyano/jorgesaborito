<?php defined("INDEX") or die(); 
Base::$body .= <<<'PHP_HTML_OUTPUT'


PHP_HTML_OUTPUT;

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Perfil de usuario</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("profile", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'
		<div class="user_functions">
			<h3>Bandeja de entrada</h3>
			<div id="messages">
			
PHP_HTML_OUTPUT;

foreach ($params['messages'] as $message) {
	Base::$body .= '<h3 onClick="read($(this))" data-read="' . $message['read'] . '" data-id_message="' . $message['id_message'] . '">' . $message['subject'] . '</h3>';
	Base::$body .= '<div><div class="message_header">'
	.'<div class="message_icons">'
	.'<a href="index.php?controller=profile&amp;action=Write"><img src="http://www.iconsdb.com/icons/preview/raspberry-red/arrow-58-m.png" alt="Responder" /></a>'
	.'<a href="javascript:;" onClick="unread($(this).parent().parent().parent().prev(\'h3\'))"><img src="http://www.iconsdb.com/icons/preview/raspberry-red/warning-m.png" alt="Responder" /></a>'
	.'<a href="javascript:;" onClick="remove($(this).parent().parent().parent().prev(\'h3\'))"><img src="http://www.iconsdb.com/icons/preview/raspberry-red/x-mark-m.png" alt="Responder" /></a>'
	.'</div>'
	.'<span class="message_subtitle">Fecha: </span>' . $message['date'] . '</div>'
	.'<div class="message_body">' . $message['message'] .'</div></div>';
}

Base::$body .= <<<'PHP_HTML_OUTPUT'
			</div>
		</div>
	</div>
</div>

PHP_HTML_OUTPUT;
?>