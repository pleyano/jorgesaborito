<?php defined("INDEX") or die(); 

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Perfil de usuario</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("profile", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'

		<div class="user_functions">

			<h3>Mis programas</h3>
			<div class="user_function_content">
					<table class="table_users" style="width: 655px; margin: 0px auto;">
<tr><th class="big_column">Fecha</th><th class="normal_column">Usuario</th><th class="smaller_column">Plan de<br />nutricion</th><th class="smaller_column">Actividad<br />fisica</th><th class="smallest_column">Estado</th><th class="smallest_column">PDF</th></tr>
PHP_HTML_OUTPUT;

foreach ($params['programs'] as $program) {
	Base::$body .= "<tr><td><a href='index.php?controller=profile&amp;action=ProgramSummary&amp;id_program=" . $program['id_program'] . "'>" . $program['date'] . "</a></td><td><a href='index.php?controller=profile&amp;action=ProgramSummary&amp;id_program=" . $program['id_program'] . "'>" . $program['username'] . "</a></td><td>";
	
	if ($program['diet'] == "0") {
		Base::$body .= "<a href='index.php?controller=profile&amp;action=ProgramSummary&amp;id_program=" . $program['id_program'] . "'><img src='images/program_act_die_status" . $program['diet'] . ".png' alt='' /></a>";
	} else {
		Base::$body .= "<a href='index.php?controller=profile&amp;action=ProgramDiet&amp;id_program=" . $program['id_program'] . "'><img src='images/program_act_die_status" . $program['diet'] . ".png' alt='' /></a>";
	}
	Base::$body .= "</td><td>";
	if ($program['activity'] == "0") {
		Base::$body .= "<a href='index.php?controller=profile&amp;action=ProgramSummary&amp;id_program=" . $program['id_program'] . "'><img src='images/program_act_die_status" . $program['activity'] . ".png' alt='' /></a>";
	} else {
		Base::$body .= "<a href='index.php?controller=profile&amp;action=ProgramActivity&amp;id_program=" . $program['id_program'] . "'><img src='images/program_act_die_status" . $program['activity'] . ".png' alt='' /></a>";
	}
	Base::$body .= "</td><td>";
	switch ($program['status']) {
		case "1":
		Base::$body .= "<a href='javascript:;'><img src='images/program_act_die_status" . $program['status'] . ".png' alt='' title='Confirmado por el usuario'/></a>";
		break;
		
		case "2":
		Base::$body .= "<a href='index.php?controller=profile&amp;action=ProgramSummary&amp;id_program=" . $program['id_program'] . "'><img src='images/program_act_die_status" . $program['status'] . ".png' alt='' title='Pago confirmado'/></a>";
		break;
		
		case "3":
		Base::$body .= "<a href='index.php?controller=profile&amp;action=ProgramSummary&amp;id_program=" . $program['id_program'] . "'><img src='images/program_act_die_status" . $program['status'] . ".png' alt='' title='Exento de pago'/></a>";
		break;
		
		default:
		Base::$body .= "<a href='index.php?controller=profile&amp;action=ProgramSummary&amp;id_program=" . $program['id_program'] . "'><img src='images/program_act_die_status" . $program['status'] . ".png' alt='' title='Pendiente de pago'/></a>";
		break;
	}
	Base::$body .= "</td><td>";
	if ($program['pdf'] == "0") {
		Base::$body .= "<a href='javascript:;'><img src='images/pdf0.png' alt=''></a>";
	} else {
		Base::$body .= "<a href='". $program['pdf'] . "'><img src='images/pdf1.png' alt=''></a>";
	}
	
	Base::$body .= "</td></tr>\n";
}

Base::$body .= <<<'PHP_HTML_OUTPUT'
					
					
					</table>
				
			</div>
		</div>
	</div>
</div>
<script>
	$("a img").tooltip({
		track: true,
		show: false,
		hide: false,
		tooltipClass: "label_info_question_tooltip",
		position: { 
			at: 'center bottom', 
			my: 'center top+15'
	}});
</script>

PHP_HTML_OUTPUT;
?>