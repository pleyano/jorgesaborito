<?php defined("INDEX") or die(); 
Base::$body .= <<<'PHP_HTML_OUTPUT'


PHP_HTML_OUTPUT;

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Perfil de usuario</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("profile", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'

		<div class="user_functions">

			<h3>Nuevo programa</h3>
			<div class="program_section">
				
PHP_HTML_OUTPUT;

Base::$body .= '<form action="index.php?controller=profile&amp;action=Newprogram04" method="post">';

Base::$body .= <<<'PHP_HTML_OUTPUT'

					<h4>Actividad física</h4>
					
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus at libero et magna sodales congue. Praesent ultrices ipsum et consequat egestas. In feugiat tempor pharetra. Nullam commodo.</p>
					<div style="text-align: left; padding: 30px 0px;">
						<table class="activities_form">
							<tr>
								<td class="activity_name">Tipo de actividad</td>
								<td class="activity_duration">Duración</td>
								<td class="activity_frequency">Veces/Semana</td>
								<td></td>
							</tr>
							<tr>
								<td><input class="changing_form_input value_default" type="text" name="activity_name[]" /></td>
								<td>
									<select name="activity_duration[]">
										<option value="">Duración...</option>
										<option value="1">&lt 30min</option>
										<option value="2">30min - 1h</option>
										<option value="3">1h - 1h 30min</option>
										<option value="4">1h 30min - 2h</option>
										<option value="5">&gt; 2h</option>
									</select>
								</td>
								<td><input class="input_smallest changing_form_input value_default" type="text" name="activity_frequency[]" /></td>
								<td><span class="row_cross button_style" onClick="javascript:deleteRow($(this).parent().parent());">X</span></td>
							</tr>
						</table>
						<span class="button_style" onClick="addRow()">Nueva actividad +</span>
						
					<h4 style="padding-top: 30px">Observaciones</h4>
					<textarea name="observations" class="textarea_more_info changing_form_input value_default input_full_size" style="height: 200px;"></textarea>
					
					</div>
					<input type="submit" name="form_activity_submit" value="Siguiente" />
				</form>
			</div>
		</div>
	</div>
</div>

<script>

	//$("select.activity_duration:last").msDropDown()
	function deleteRow(element) {
		if ($(".activities_form tr").size() > 2) {
			element.remove();
		}
	}
	
	var sample_row = $(".activities_form tr:nth-of-type(2)").clone();
	
	function addRow() {
		sample_row.clone().appendTo(".activities_form");
		//$(".activity_duration:last").msDropDown();
	}
	
</script>
PHP_HTML_OUTPUT;
?>