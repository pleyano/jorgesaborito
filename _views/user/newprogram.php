<?php defined("INDEX") or die(); 
Base::$body .= <<<'PHP_HTML_OUTPUT'


PHP_HTML_OUTPUT;

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Perfil de usuario</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("profile", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'

		<div class="user_functions">

			<h3>Nuevo programa</h3>
			<div class="program_section">
				<h4>¿Dieta, actividad física o ambos?</h4>
				<div class="big_columns_wrapper">
					<a href="index.php?controller=profile&amp;action=Newprogram01" class="big_column_buttons">DIETA</a>
					<a href="index.php?controller=profile&amp;action=Newprogram02" class="big_column_buttons">AMBOS</a>
					<a href="index.php?controller=profile&amp;action=Newprogram03" class="big_column_buttons">ACTIVIDAD FISICA</a>
				</div>
			</div>
		</div>
	</div>
</div>

PHP_HTML_OUTPUT;
?>