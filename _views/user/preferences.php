<?php defined("INDEX") or die(); 

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Perfil de usuario</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("profile", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'

		<div class="user_functions">

			<h3>Preferencias</h3>
			<div class="program_section">
			<form action="index.php?controller=profile&amp;action=Preferences" method="post" id="preferences_form">
PHP_HTML_OUTPUT;
Base::$body .= $params['error'];
Base::$body .= <<<'PHP_HTML_OUTPUT'

				<h4>Mi cuenta</h4>
				<div>
					<div class="data_section_hor">
						<label>Nombre de usuario: </label><input class="data_section_input changing_form_input" value="
PHP_HTML_OUTPUT;
Base::$body .= $params['user_preferences']['username'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
" type="text" disabled="disabled" />
					</div>
					<div class="data_section_hor">
						<label>Nuevo Email: </label><input class="data_section_input changing_form_input input_normal" type="text" id="email" name="email" />
					</div>
					<div class="data_section_hor">
						<label class="label_info" title="Solo tienes que rellenar esto en caso de que quieras cambiar tu Email">Confirmar Email: </label><input class="data_section_input changing_form_input input_normal" type="text" id="my_email2" name="my_email2" />
					</div>
					<div class="data_section_hor">
						<label>Nueva contraseña: </label><input class="data_section_input changing_form_input" type="password" id="password" name="password" />
					</div>
					<div class="data_section_hor">
						<label class="label_info" title="Solo tienes que rellenar esto en caso de que quieras cambiar tu contraseña">Confirmar contraseña: </label><input class="data_section_input changing_form_input" type="password" id="my_new_password2" name="my_new_password2" />
					</div>
PHP_HTML_OUTPUT;
/*
					<div class="data_section_hor">
						<label class="label_info" title="La imagen debe medir un máximo de 150x150 y tener un peso inferior a 100kb">Avatar: </label><div class="data_section_input upload_div" >
						<img src="
PHP_HTML_OUTPUT;
Base::$body .= $params['user_preferences']['avatar_path'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
" alt="" class="current_avatar" /><br /><input class="data_section_input changing_form_input" data-custom_value="
PHP_HTML_OUTPUT;
Base::$body .= $params['user_preferences']['avatar'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
" type="file" name="avatar" /></div>
					</div>
				</div>
*/
Base::$body .= <<<'PHP_HTML_OUTPUT'
				<h4 style="padding: 40px 0px 30px;">Confirmación</h4>
					<div class="data_section_hor">
						<label class="label_info" title="Por cuestiones de seguridad, por favor, introduzca su contraseña actual para realizar cualquier cambio">Contraseña actual: </label><input class="data_section_input changing_form_input" id="my_password" type="password" name="my_password" />
					</div>
				<input type="submit" style="margin: 30px 0px 5px" name="update_preferences" id="form_submit" value="Actualizar informacion" />
				</form>
				<script>
					$("#preferences_form").submit(function() {
						if ($("#password").val() != $("#my_new_password2").val()) {
							return false;
						}
						if ($("#email").val() != $("#my_email2").val()) {
							return false;
						}
						if ($("#my_password").val() == "") {
							return false;
						}
					});
					$(".label_info").append(function(index, html) {
						return "<span class='label_info_question' title='" + $(this).attr("title") + "'>?<span>";
					});
					$(".label_info_question").tooltip({
						track: true,
						show: false,
						hide: false,
						tooltipClass: "label_info_question_tooltip",
						position: { 
							at: 'center bottom', 
							my: 'center top+15'
					}});
					
				</script>
			</div>
		</div>
	</div>
</div>

PHP_HTML_OUTPUT;
?>