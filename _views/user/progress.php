<?php defined("INDEX") or die(); 
Base::$body .= <<<'PHP_HTML_OUTPUT'


PHP_HTML_OUTPUT;

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Perfil de usuario</h2>
	</div>
	
		<div class="wrap_profile wrap">
		
PHP_HTML_OUTPUT;

Base::view("profile", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'
		<div class="user_functions">
			<h3>Mi progreso</h3> 
			<div class="user_function_content">
			
			
			<h4>Peso</h4>
			<div class="chart">
				<div id="chart_weight"></div>
			</div>
			<script>
PHP_HTML_OUTPUT;

Base::$body .= "\n\t\t\tvar chart_weight_miny =  " . $params['chart_weight_miny'] . ";";
Base::$body .= "\n\t\t\tvar chart_weight_maxy =  " . $params['chart_weight_maxy'] . ";";
Base::$body .= "\n\t\t\tvar chart_weight_weights =  [";
for ($i = 0; $i < sizeof($params['chart_weight_weights']) - 1; $i++) {
	Base::$body .= $params['chart_weight_weights'][$i] . ', ';
}
Base::$body .= $params['chart_weight_weights'][$i] . '];';

Base::$body .= "\n\t\t\tvar chart_weight_dates = [";

for ($i = 0; $i < sizeof($params['chart_weight_dates']) - 1; $i++) {
	Base::$body .= '"' . $params['chart_weight_dates'][$i] . '", ';
}
Base::$body .= '"' . $params['chart_weight_dates'][$i] . '"];';

Base::$body .= <<<'PHP_HTML_OUTPUT'

			var djData = {
			   "Fecha": chart_weight_dates,
			   "Peso": chart_weight_weights
			}
			var polyjsdata = polyjs.data(djData);
			var spec = {
				layers: [
				{
					data: polyjsdata,
					type: 'line',
					x: 'Fecha',
					y: 'Peso',
					size: {'const': 3}
				},
				{
					data: polyjsdata,
					type: 'point',
					x: 'Fecha',
					y: 'Peso',
					size: {'const': 5},
					opacity: {'const': 1}, 
					tooltip: function(item) { return item.Peso.toFixed(2)}
				}], 
				guide: {
					x: {
						numticks: chart_weight_dates.length
					},
					y: {
						min: chart_weight_miny,
						max: chart_weight_maxy,
						numticks: 10
					}
				},
				dom: 'chart_weight',
				width: 760,
				height: 400
			}
			polyjs.chart(spec);

			</script>
			
			
			<h4>Índice de Masa Corporal</h4>
			<div class="chart">
				<div id="chart_imc"></div>
			</div>
			<script>
PHP_HTML_OUTPUT;

Base::$body .= "\n\t\t\tvar chart_imc_miny =  " . $params['chart_imc_miny'] . ";";
Base::$body .= "\n\t\t\tvar chart_imc_maxy =  " . $params['chart_imc_maxy'] . ";";
Base::$body .= "\n\t\t\tvar chart_imc_imcs =  [";
for ($i = 0; $i < sizeof($params['chart_imc_imcs']) - 1; $i++) {
	Base::$body .= $params['chart_imc_imcs'][$i] . ', ';
}
Base::$body .= $params['chart_imc_imcs'][$i] . '];';

Base::$body .= "\n\t\t\tvar chart_imc_dates = [";

for ($i = 0; $i < sizeof($params['chart_imc_dates']) - 1; $i++) {
	Base::$body .= '"' . $params['chart_imc_dates'][$i] . '", ';
}
Base::$body .= '"' . $params['chart_imc_dates'][$i] . '"];';

Base::$body .= <<<'PHP_HTML_OUTPUT'

			var djData = {
			   "Fecha": chart_imc_dates,
			   "IMC": chart_imc_imcs
			}
			var polyjsdata = polyjs.data(djData);
			var spec = {
				layers: [
				{
					data: polyjsdata,
					type: 'line',
					x: 'Fecha',
					y: 'IMC',
					size: {'const': 3}
				},
				{
					data: polyjsdata,
					type: 'point',
					x: 'Fecha',
					y: 'IMC',
					size: {'const': 5},
					opacity: {'const': 1}, 
					tooltip: function(item) { return item.IMC.toFixed(2)}
				}], 
				guide: {
					x: {
						numticks: chart_imc_dates.length
					},
					y: {
						min: chart_imc_miny,
						max: chart_imc_maxy,
						numticks: 10
					}
				},
				dom: 'chart_imc',
				width: 760,
				height: 400
			}
			polyjs.chart(spec);

			</script>
			
			<h4>Porcentaje de tejido graso</h4>
			<div class="chart">
				<div id="chart_ptg"></div>
			</div>
			<script>
PHP_HTML_OUTPUT;

Base::$body .= "\n\t\t\tvar chart_ptg_miny =  " . $params['chart_ptg_miny'] . ";";
Base::$body .= "\n\t\t\tvar chart_ptg_maxy =  " . $params['chart_ptg_maxy'] . ";";
Base::$body .= "\n\t\t\tvar chart_ptg_ptgs =  [";
for ($i = 0; $i < sizeof($params['chart_ptg_ptgs']) - 1; $i++) {
	Base::$body .= $params['chart_ptg_ptgs'][$i] . ', ';
}
Base::$body .= $params['chart_ptg_ptgs'][$i] . '];';

Base::$body .= "\n\t\t\tvar chart_ptg_dates = [";

for ($i = 0; $i < sizeof($params['chart_ptg_dates']) - 1; $i++) {
	Base::$body .= '"' . $params['chart_ptg_dates'][$i] . '", ';
}
Base::$body .= '"' . $params['chart_ptg_dates'][$i] . '"];';

Base::$body .= <<<'PHP_HTML_OUTPUT'

			var djData = {
			   "Fecha": chart_ptg_dates,
			   "PTG": chart_ptg_ptgs
			}
			var polyjsdata = polyjs.data(djData);
			var spec = {
				layers: [
				{
					data: polyjsdata,
					type: 'line',
					x: 'Fecha',
					y: 'PTG',
					size: {'const': 3}
				},
				{
					data: polyjsdata,
					type: 'point',
					x: 'Fecha',
					y: 'PTG',
					size: {'const': 5},
					opacity: {'const': 1}, 
					tooltip: function(item) { return item.PTG.toFixed(2) + "%"}
				}], 
				guide: {
					x: {
						numticks: chart_ptg_dates.length
					},
					y: {
						min: chart_ptg_miny,
						max: chart_ptg_maxy,
						numticks: 10
					}
				},
				dom: 'chart_ptg',
				width: 760,
				height: 400
			}
			polyjs.chart(spec);

			</script>
			
			
			
			</div>
		</div>
	</div>
</div>

PHP_HTML_OUTPUT;
?>