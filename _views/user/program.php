<?php defined("INDEX") or die(); 

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Perfil de usuario</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("profile", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'

		<div class="user_functions">

			<h3>Programa</h3>
			<ul class="user_program_sections">
				<li><a href="index.php?controller=profile&amp;action=ProgramSummary&amp;id_program=
PHP_HTML_OUTPUT;

Base::$body .= $_GET['id_program'];

Base::$body .= <<<'PHP_HTML_OUTPUT'
">General</a></li>
PHP_HTML_OUTPUT;

if (!empty($params['program_summary']['diet'])) {
	Base::$body .= "<li><a href='index.php?controller=profile&amp;action=ProgramDiet&amp;id_program=" . $_GET['id_program'] . "'>Dieta</a></li>";
} else {
	Base::$body .= "<li><a class='disabled' href='javascript:;'>Dieta</a></li>";
}

if (!empty($params['program_summary']['activity'])) {
	Base::$body .= "<li><a href='index.php?controller=profile&amp;action=ProgramActivity&amp;id_program=" . $_GET['id_program'] ."'>Actividad física</a></li>";
} else {
	Base::$body .= "<li><a class='disabled' href='javascript:;'>Actividad física</a></li>";
}
Base::$body .= <<<'PHP_HTML_OUTPUT'
			</ul>
			<div class="user_function_content">
				<div class="user_section" stlye="overflow: auto">
					<h4>Información</h4>
					<span class="profile_summary_title" style="line-height: 25px;">Identificador:</span> 
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['program_summary']['id_program'])) ? $params['program_summary']['id_program'] : "No hay información";

Base::$body .= <<<'PHP_HTML_OUTPUT'

					<br />
					<span class="profile_summary_title" style="line-height: 25px;">Estudio nutricional:</span> 
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['program_summary']['diet'])) ? "Si" : "No";

Base::$body .= <<<'PHP_HTML_OUTPUT'

					<br />
					<span class="profile_summary_title" style="line-height: 25px;">Plan de actividad física:</span> 
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['program_summary']['activity'])) ? "Si" : "No";

Base::$body .= <<<'PHP_HTML_OUTPUT'

					<br />
					<span class="profile_summary_title" style="line-height: 25px;">Fecha:</span> 
PHP_HTML_OUTPUT;

Base::$body .= (!empty($params['program_summary']['date'])) ? $params['program_summary']['date'] : "No hay información";

Base::$body .= <<<'PHP_HTML_OUTPUT'

					<br />
					<span class="profile_summary_title" style="line-height: 25px;">Estado:</span> 
PHP_HTML_OUTPUT;

switch ($params['program_summary']['payment_status']) {

	case "1":
		Base::$body .= "Confirmado por el usuario";
		break;
		
	case "2":
		Base::$body .= "Pago confirmado";
		break;
		
	case "3":
		Base::$body .= "Exento de pago";
		break;
		
	default:
		Base::$body .= "Pendiente de pago";
		break;
}
Base::$body .= <<<'PHP_HTML_OUTPUT'

					<br />
					<span class="profile_summary_title" style="line-height: 25px;">PDF del estudio:</span>  
PHP_HTML_OUTPUT;

Base::$body .= (($params['program_summary']['asociated_pdf']) != "0") ? ("<a href='" . $params['program_summary']['asociated_pdf'] . "'>Descargar</a>") : "No hay información";

Base::$body .= <<<'PHP_HTML_OUTPUT'
				
					<h4 style="padding-top: 20px">Acciones</h4>
					<div class="summary_buttons">
						<div>
PHP_HTML_OUTPUT;
$any_action = false;
if ($params['program_summary']['payment_status'] == "0") {
	Base::$body .= 	"<div class='program_summary_button payment_button'><a href='index.php?controller=profile&amp;action=ConfirmPayment&amp;id_program=" . $_GET['id_program'] . "'>Confirmar pago</a></div>";
	$any_action = true;
}
Base::$body .= <<<'PHP_HTML_OUTPUT'
PHP_HTML_OUTPUT;
if ($params['program_summary']['asociated_pdf'] != "0") {
	Base::$body .= 	"<div class='program_summary_button pdf_button'><a href='" . $params['program_summary']['asociated_pdf'] . "'>Descargar PDF</a></div>";
	$any_action = true;
}
if ($any_action === false) {
	Base::$body .= "No hay acciones disponibles";
}
Base::$body .= <<<'PHP_HTML_OUTPUT'
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

PHP_HTML_OUTPUT;
?>