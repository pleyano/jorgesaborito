<?php defined("INDEX") or die(); 

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Perfil de usuario</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("profile", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'

		<div class="user_functions">

			<h3>Mi ficha de datos</h3>
			<div class="program_section">
				<h4>Usuario</h4>
				<form action="index.php?controller=profile&amp;action=Data" method="post">
				<div class="data_left_column">
					<div class="data_section">
						<label>Nombre: </label><input class="data_section_input changing_form_input input_normal" value="
PHP_HTML_OUTPUT;
Base::$body .= $params['data']['name'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
" type="text" name="name" />
					</div>
					<div class="data_section">
						<label>Fecha de nacimiento: </label><input class="data_section_input changing_form_input" id="date" value="
PHP_HTML_OUTPUT;
Base::$body .= $params['data']['birthday'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
" type="text" name="birthday" />
					</div>
					<div class="data_section">
						<label>Sexo: </label><input type="radio" name="gender" value="m"
PHP_HTML_OUTPUT;
Base::$body .= ($params['data']['gender'] == "m") ? ' checked="checked" ' : '';
Base::$body .= <<<'PHP_HTML_OUTPUT'
 /> M <input type="radio" name="gender" value="f"
PHP_HTML_OUTPUT;
Base::$body .= ($params['data']['gender'] == "f") ? ' checked="checked" ' : '';
Base::$body .= <<<'PHP_HTML_OUTPUT'
 /> H  
					</div>
					<div class="data_section">
						<label>Sensacion de hambre: </label><div id="hunger_slide" class="input_normal"></div><input type="text" value="
PHP_HTML_OUTPUT;
Base::$body .= $params['data']['hunger'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
" class="hidden_field" name="hunger" id="hunger" />
					</div>
				</div>
				<div class="data_right_column">
					<div class="data_section">
						<label>Peso (kg): </label><input class="data_section_input changing_form_input" type="text" value="
PHP_HTML_OUTPUT;
Base::$body .= $params['data']['weight'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
" id="weight" name="weight" />
					</div>
					<div class="data_section">
						<label>Altura (cm): </label><input class="data_section_input changing_form_input" type="text" value="
PHP_HTML_OUTPUT;
Base::$body .= $params['data']['height'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
" id="height" name="height" />
					</div>
					<div class="data_section">
						<label class="label_info" title="Indice de Masa Corporal">IMC: </label><input class="data_section_input changing_form_input" type="text" id="imc" disabled="disabled"/>
					</div>
					<div class="data_section">
						<label>Actividad física: </label>
						<select name="activity" id="activity_select">
							<option value="1"
PHP_HTML_OUTPUT;
Base::$body .= ($params['data']['activity'] == 1) ? ' selected="selected" ' : '';
Base::$body .= <<<'PHP_HTML_OUTPUT'
>Nada</option>
							<option value="2"
PHP_HTML_OUTPUT;
Base::$body .= ($params['data']['activity'] == 2) ? ' selected="selected" ' : '';
Base::$body .= <<<'PHP_HTML_OUTPUT'
>Poca</option>
							<option value="3"
PHP_HTML_OUTPUT;
Base::$body .= ($params['data']['activity'] == 3) ? ' selected="selected" ' : '';
Base::$body .= <<<'PHP_HTML_OUTPUT'
>Media</option>
							<option value="4"
PHP_HTML_OUTPUT;
Base::$body .= ($params['data']['activity'] == 4) ? ' selected="selected" ' : '';
Base::$body .= <<<'PHP_HTML_OUTPUT'
>Elevada</option>
						</select>
					</div>
				</div>
				<h4 style="padding: 40px 0px 30px;">Medidas individuales</h4>
				
				<div class="data_left_column">
					<div class="data_section_hor">
						<label class="label_info" title="Rodear el cuerpo con la cinta a la altura de del ombligo, de manera que ésta se mantenga pareja a la altura de todo el recorrido">Controrno de la cintura (cm): </label><input class="data_section_input changing_form_input input_smallest" type="text" value="
PHP_HTML_OUTPUT;
Base::$body .= $params['data']['waist'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
" name="waist" id="waist" />
					</div>
					<div class="data_section_hor">
						<label class="label_info" title="Con los pies juntos, medir alrededor de la circunferencia mas grande de las caderas">Contorno de la cadera (cm): </label><input class="data_section_input changing_form_input input_smallest" type="text" value="
PHP_HTML_OUTPUT;
Base::$body .= $params['data']['hips'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
" name="hips" id="hips" />
					</div>
					<div class="data_section_hor">
						<label class="label_info" title="Porcentaje de Tejido Graso">PTG: </label><input class="data_section_input changing_form_input input_smallest" type="text" id="ptg" disabled="disabled" />
					</div>
				</div>
				<div class="data_right_column">
					<div class="data_section_hor">
						<label class="label_info" title="Medir por la parte más ancha del muslo">Perímetro del muslo (cm): </label><input class="data_section_input changing_form_input input_smallest" value="
PHP_HTML_OUTPUT;
Base::$body .= $params['data']['leg'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
" type="text" name="leg" id="leg" />
					</div>
					<div class="data_section_hor">
						<label class="label_info" title="Medir por la parte más ancha del brazo">Perímetro del brazo (cm): </label><input class="data_section_input changing_form_input input_smallest" type="text" value="
PHP_HTML_OUTPUT;
Base::$body .= $params['data']['arm'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
" name="arm" id="arm" />
					</div>
				</div>
				
				<h4 style="padding: 40px 0px 30px;">Otros datos</h4>
				<div>
					<div class="data_section">
						<label>Enfermedades: </label><textarea name="illnesses" class="textarea_full_size textarea_more_info changing_form_input">
PHP_HTML_OUTPUT;
Base::$body .= $params['data']['illnesses'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
</textarea>
					</div>
					<div class="data_section">
						<label>Alergias: </label><textarea name="allergies" class="textarea_full_size textarea_more_info changing_form_input">
PHP_HTML_OUTPUT;
Base::$body .= $params['data']['allergies'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
</textarea>
					</div>
					<div class="data_section">
						<label>Otros datos: </label><textarea name="other" class="textarea_full_size textarea_more_info changing_form_input">
PHP_HTML_OUTPUT;
Base::$body .= $params['data']['other'];
Base::$body .= <<<'PHP_HTML_OUTPUT'
</textarea>
					</div>
				</div>
				<input name="update_data" type="submit" style="margin: 20px 0px 5px" value="Actualizar informacion" />
				</form>
			</div>
		</div>
	</div>
</div>

<script>

	function calcIMC() {
		var result = Math.round(parseFloat($("#weight").val()) / (parseInt($("#height").val()) * parseInt($("#height").val())) * 1000000)/100;
		if (result > 5)
			$("#imc").val(result);	
		else 
			$("#imc").val("0.00");
	
	}
	
	function calcPTG() {
		var result = Math.round((parseInt($("#leg").val()) + parseInt($("#arm").val()) + parseInt($("#waist").val()) + parseInt($("#hips").val()))/3*100)/100;
		if (result > 5)
			$("#ptg").val(result);	
		else 
			$("#ptg").val("0.00");
	}
	
	calcIMC();
	calcPTG();

	$(".label_info").append(function(index, html) {
		return "<span class='label_info_question' title='" + $(this).attr("title") + "'>?<span>";
	});
	$(".label_info_question").tooltip({
		track: true,
		show: false,
		hide: false,
		tooltipClass: "label_info_question_tooltip",
		position: { 
			at: 'center bottom', 
			my: 'center top+15'
	}});
	$("#hunger_slide").slider({
		min: 1,
		max: 10,
		step: 1,
		range: "min",
		value: $("#hunger").val(),
		slide: function(event, ui) {
			$("#hunger").val(ui.value);
		}
	});
	$("#date").datepicker(); 
	$("#activity_select").msDropDown();
	$("#weight, #height").change(function() {calcIMC();});
	$("#leg, #arm, #waist, #hips").change(function() {calcPTG();});
	
	
</script>
				
PHP_HTML_OUTPUT;
?>