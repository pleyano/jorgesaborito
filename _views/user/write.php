<?php defined("INDEX") or die(); 
Base::$body .= <<<'PHP_HTML_OUTPUT'


PHP_HTML_OUTPUT;

Base::view("profile", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="user_profile">
	<div class="wrap">
	<h2>Perfil de usuario</h2>
	</div>
	
	<div class="wrap_profile wrap">
PHP_HTML_OUTPUT;

Base::view("profile", "Features");

Base::$body .= <<<'PHP_HTML_OUTPUT'
		<div class="user_functions">
			<h3>Enviar Mensaje</h3>
			<div id="messages">
				<form action="index.php?controller=profile&amp;action=Write" method="post">
				<div class="data_section">
					<label>Asunto:</label>
					<input type="text" name="subject" class="input_full_size changing_form_input" />
				</div>
				<div class="data_section">
					<label>Mensaje:</label>
					<textarea id="message_box" name="content"></textarea>
				</div>
				<input name="send_message" type="submit" style="margin: 20px 0px 5px"  value="Enviar mensaje" />
				</form>
			</div>
		</div>
	</div>
</div>
<script>
$("#message_box").wysiwyg({
	autoGrow:true, 
	controls: "bold,italic,underline,strikeThrough,|,justifyLeft,justifyCenter,justifyRight,justifyFull,|,insertOrderedList,insertUnorderedList,insertHorizontalRule",
	initialContent: ""
});
$("form").submit(function() {
	$("#message_box").wysiwyg("saveContent");
	return true;
})
</script>
PHP_HTML_OUTPUT;
?>