<?php defined("INDEX") or die(); 
Base::$body .= <<<'PHP_HTML_OUTPUT'


PHP_HTML_OUTPUT;

Base::view("index", "Header");

Base::$body .= <<<'PHP_HTML_OUTPUT'


<div class="content">
<div class="wrap">
<div class="login_content">

PHP_HTML_OUTPUT;

if (isset($params['login_error'])) 
	Base::$body .= "<h2 class='login_error'>" . $params['login_error'] . "</h2>";

Base::$body .= <<<'PHP_HTML_OUTPUT'

<form action="index.php?controller=index&amp;action=Login" method="post">

	<input type="text" class="input_default changing_form_input" value="Usuario" name="quick_username" />
	<br />
	<input type="password" class="input_default changing_form_input" value="Contrase&ntilde;a" name="quick_password" />
	<br />
	<input type="submit" name="quick_submit" value="Entrar" />

</form>

<div class="login_options"><a href="#">Olvide mi contraseña</a> | <a href="index.php?controller=index&amp;action=Register">Registrarse</a></div>

</div>
</div>
</div>

PHP_HTML_OUTPUT;

Base::view("index", "Footer");

Base::$body .= <<<'PHP_HTML_OUTPUT'

PHP_HTML_OUTPUT;
?>