$(document).ready(function() {
	$( "#messages" ).accordion({
		collapsible: true,
		active: false
	});
	
	$("h3").each(function() {
		if ($(this).attr("data-read")) {
			if ($(this).attr("data-read") == "0") {
				$(this).addClass("unread_message");
			} else {
				$(this).addClass("read_message");
			}
		}
	});
});

function read(item) {

	var id_message = $(item).attr("data-id_message");
	if ($(item).attr("data-read") == "0") {
		$.ajax("index.php?controller=ajax&action=Read&id_message=" + id_message).done(function ( data ) {
			$(item).removeClass("unread_message");
			$(item).addClass("read_message");
			$(item).attr("data-read", "1");
			refresh_top_menu(-1);
		})
	}

}

function unread(item) {
	var id_message = $(item).attr("data-id_message");
	$(item).addClass("unread_message");
	$(item).removeClass("read_message");
	if ($(item).attr("data-read") == "1") {
		$.ajax("index.php?controller=ajax&action=Unread&id_message=" + id_message).done(function () {
			$("#messages").accordion({active: false});
			$(item).attr("data-read", "0");
			refresh_top_menu(1);
		})
	}

}

function remove(item) {
	var id_message = $(item).attr("data-id_message");
	$.ajax("index.php?controller=ajax&action=Remove&id_message=" + id_message).done(function () {
		$(item).next().remove();
		$(item).remove();
	})

}

function selfRemove(item) {
	var id_message = $(item).attr("data-id_message");
	$.ajax("index.php?controller=ajax&action=SelfRemove&id_message=" + id_message).done(function () {
		$(item).next().remove();
		$(item).remove();
	})

}

function refresh_top_menu(q) {
		$("#top_menu_unread_messages").text(parseInt($("#top_menu_unread_messages").text())+q);
		if ($("#top_menu_unread_messages").text() == "0") {
			$("#top_menu_unread_messages").css("display", "none");
		} else {
			$("#top_menu_unread_messages").css("display", "inline-block");
		}
}