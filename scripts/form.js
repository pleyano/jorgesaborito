$(document).ready(function() {
	$("input[type='file']").wrap('<div class="contains_input_file">').before(function() {
		return '<input type="text" class="data_section_input changing_form_input" value="' + $(this).attr("data-custom_value") + '"/>';
	}).parent().addClass("contains_input_file");
	$("input[type='file']").change(function() { 
		$(this).parent().children('input[type="text"]').val($(this).val()).addClass("value_not_equal_to_default");; 

	})
	$("input, textarea").addClass("value_default");

	$("input[type='text'], textarea").blur(function() {
		if ($(this).val() == "") {
			$(this).removeClass("value_not_equal_to_default");
		} else {
			if ($(this).val() != $(this).attr("data-default")) {
				$(this).addClass("value_not_equal_to_default");
			} else {
				$(this).removeClass("value_not_equal_to_default");
			}
		}
	});
	$.each($("input, textarea"), function() {
		$(this).attr("data-default", $(this).val());
	});
});