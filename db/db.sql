create database jorgesaborito;

use jorgesaborito;

create table js_users (
	id_user int(11) auto_increment primary key,
	username varchar(255) not null,
	password varchar(255) not null,
	email varchar(255) not null,
	active varchar(32) not null,
	regist_date timestamp,
	last_connection datetime,
	notifiactions boolean,
	name varchar(255),
	birthday date,
	gender varchar(1),
	illnesses varchar(1023),
	allergies varchar(1023),
	other varchar(1023),
	evaluation varchar(2047)

) Engine=InnoDB;

create table js_history (
	id_user int(11) references js_users(id_user),
	date timestamp,
	weight decimal(9,2) not null,
	height int(11) not null,
	imc decimal(7,4),
	ptg decimal(7, 4),
	leg int(11),
	arm int(11),
	waist int(11),
	hips int(11),
	hunger int(11),
	activity int(11),
	primary key (id_user, date)
	
) Engine = InnoDB;

create table js_roles (
	id_role int(11) unsigned auto_increment primary key,
	name varchar(255) not null
) Engine=InnoDB;

create table js_privileges (
	id_user int(11) references js_users(id_user),
	id_role int(11) references js_roles(id_role),
	primary key (id_user, id_role)
) Engine=InnoDB;

create table js_messages (
	id_message int(11) auto_increment primary key,
	id_author int(11) references js_users(id_user),
	id_recipent int(11) references js_users(id_user),
	subject varchar(255),
	content varchar(255),
	date datetime,
	recipent_read boolean,
	attached_files varchar(4095),
	response_to int(11),
	deleted boolean
	
) Engine = InnoDB;

create table js_diets (

	id_diet int(11) auto_increment primary key,
	day1 varchar(15),
	day2 varchar(15),
	day3 varchar(15),
	day4 varchar(15),
	day5 varchar(15),
	day6 varchar(15),
	day7 varchar(15),
	24hours1 varchar(300),
	24hours2 varchar(300),
	24hours3 varchar(300),
	24hours4 varchar(300),
	24hours5 varchar(300),
	24hours6 varchar(300),
	24hours7 varchar(300),
	observations varchar(1000)		

) Engine = InnoDB;

create table js_activities (
	
	id_activity int(11) auto_increment primary key,
	activity varchar(500),
	duration varchar(100),
	frequency varchar(100),
	observations varchar(1500)

) Engine = InnoDB;

create table js_programs (

	id_program int(11) auto_increment primary key,
	diet int(11) references js_diets(id_diet),
	activity int(11) references js_activities(id_activity),
	id_user int(11) references js_users(id_user),
	date datetime not null,
	payment_status int(2),
	asociated_pdf varchar(255)
	
) Engine = InnoDB;


# Crear roles y usuario por defecto

insert into js_roles values(default, "Administrador");
insert into js_roles values(default, "Usuario");

insert into js_users set username = "admin", password = md5("123456"), email = "info@jorgesaborito.com", active = "0";
insert into js_privileges values(1, 1);
insert into js_privileges values(1, 2);

# Este trigger asocia el rol Usuario (2) por defecto a los usuarios insertados en la tabla js_users

DELIMITER |

CREATE TRIGGER default_role AFTER INSERT ON js_users

FOR EACH ROW BEGIN
INSERT INTO js_privileges values(NEW.id_user, 2);

END

|

DELIMITER ;

insert into js_users set username = "admin", password = md5("123456"), email = "info@jorgesaborito.com", active = "0";
insert into js_privileges values(1, 1);