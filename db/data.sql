insert into js_history values (1, "2013-04-05", 80, 170, 25.2, 30, 40, 33, 41, 45, 2, 3);
insert into js_history values (1, "2013-04-06", 81, 170, 25.2, 30.3, 40, 33, 41, 45, 2, 2);
insert into js_history values (1, "2013-04-07", 80.5, 170, 25.2, 30.4, 40, 33, 41, 45, 5, 3);
insert into js_history values (1, "2013-04-09", 80.7, 170, 25, 30.5, 40, 33, 41, 45, 2, 3);
insert into js_history values (1, "2013-04-12", 80.1, 171, 25, 31, 40, 33, 41, 45, 6, 1);
insert into js_history values (1, "2013-04-13", 79, 171, 25.1, 28.8, 40, 33, 41, 45, 4, 3);
insert into js_history values (1, "2013-04-14", 76, 171, 25.0, 28.6, 40, 33, 41, 45, 3, 1);
insert into js_history values (1, "2013-04-15", 78, 171, 25.22, 28, 40, 33, 41, 45, 3, 3);
insert into js_history values (1, "2013-04-16", 79, 171, 24.5, 30, 40, 33, 41, 45, 5, 2);
insert into js_history values (1, "2013-04-17", 80, 171, 24.8, 29.7, 40, 33, 41, 45, 7, 3);
insert into js_history values (1, "2013-04-20", 80.2, 171, 25.2, 30.5, 40, 33, 41, 45, 9, 3);
insert into js_history values (1, "2013-04-22", 80.1, 171, 25.1, 31, 40, 33, 41, 45, 1, 4);

insert into js_messages values (default, 1, 2, "Mensaje de prueba", "Esto es un mensaje de prueba.", now(), 0, "", null, 0);
insert into js_messages values (default, 1, 2, "Mensaje de prueba 1", "Esto es un mensaje de prueba. Esto es un mensaje de prueba. ", now(), 0, "", null, 0);
insert into js_messages values (default, 1, 2, "Mensaje de prueba 2", "Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. ", now(), 0, "", null, 0);
insert into js_messages values (default, 1, 2, "Mensaje de prueba 3", "Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. ", now(), 0, "", null, 0);

insert into js_messages values (default, 2, 1, "Mensaje de prueba", "Esto es un mensaje de prueba.", now(), 0, "", null, 0);
insert into js_messages values (default, 2, 1, "Mensaje de prueba 1", "Esto es un mensaje de prueba. Esto es un mensaje de prueba. ", now(), 0, "", null, 0);
insert into js_messages values (default, 2, 1, "Mensaje de prueba 2", "Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. ", now(), 0, "", null, 0);
insert into js_messages values (default, 2, 1, "Mensaje de prueba 3", "Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. Esto es un mensaje de prueba. ", now(), 0, "", null, 0);