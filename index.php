<?php 
define("INDEX", true);
include("includes/config.php");

if (empty($_GET['controller']) || !file_exists("_controllers/" . $_GET['controller'] . ".php")) {
	$_GET['controller'] = 'index';
	require("_controllers/" . $_GET['controller'] . ".php");
	$_GET['action'] = 'Index';
} else {
	require("_controllers/" . $_GET['controller'] . ".php");
	if (empty($_GET['action']) || !in_array($_GET['action'], get_class_methods($_GET['controller']), true)) {
		$_GET['action'] = 'Index';
	}
}
 	
Base::view($_GET['controller'], $_GET['action']);
Base::render();

?>